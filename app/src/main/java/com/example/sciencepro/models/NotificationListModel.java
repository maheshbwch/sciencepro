package com.example.sciencepro.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotificationListModel extends BaseModel {

    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("question_id")
        @Expose
        private String questionId;
        @SerializedName("questioner_name")
        @Expose
        private String questionerName;
        @SerializedName("question_details")
        @Expose
        private String questionDetails;
        @SerializedName("answer_details")
        @Expose
        private String answerDetails;
        @SerializedName("topic_details")
        @Expose
        private String topicDetails;
        @SerializedName("quotation")
        @Expose
        private String quotation;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("reason")
        @Expose
        private String reason;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("date_time")
        @Expose
        private Integer dateTime;

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public String getQuestionerName() {
            return questionerName;
        }

        public void setQuestionerName(String questionerName) {
            this.questionerName = questionerName;
        }

        public String getQuestionDetails() {
            return questionDetails;
        }

        public void setQuestionDetails(String questionDetails) {
            this.questionDetails = questionDetails;
        }

        public String getAnswerDetails() {
            return answerDetails;
        }

        public void setAnswerDetails(String answerDetails) {
            this.answerDetails = answerDetails;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getQuotation() {
            return quotation;
        }

        public void setQuotation(String quotation) {
            this.quotation = quotation;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getDateTime() {
            return dateTime;
        }

        public void setDateTime(Integer dateTime) {
            this.dateTime = dateTime;
        }

    }
}
