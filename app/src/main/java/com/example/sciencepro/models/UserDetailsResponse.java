package com.example.sciencepro.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserDetailsResponse extends BaseModel {

    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }



    public class Datum {

        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("gender_id")
        @Expose
        private String genderId;
        @SerializedName("ic")
        @Expose
        private String ic;
        @SerializedName("university")
        @Expose
        private String university;
        @SerializedName("user_role_id")
        @Expose
        private String userRoleId;
        @SerializedName("profile_picture")
        @Expose
        private String profilePicture;
        @SerializedName("login_app")
        @Expose
        private String loginApp;
        @SerializedName("contact_no")
        @Expose
        private String contactNo;
        @SerializedName("top_up_balance")
        @Expose
        private String topUpBalance;

        @SerializedName("is_active")
        @Expose
        private String isActive;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getGenderId() {
            return genderId;
        }

        public void setGenderId(String genderId) {
            this.genderId = genderId;
        }

        public String getIc() {
            return ic;
        }

        public void setIc(String ic) {
            this.ic = ic;
        }

        public String getUniversity() {
            return university;
        }

        public void setUniversity(String university) {
            this.university = university;
        }

        public String getUserRoleId() {
            return userRoleId;
        }

        public void setUserRoleId(String userRoleId) {
            this.userRoleId = userRoleId;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getLoginApp() {
            return loginApp;
        }

        public void setLoginApp(String loginApp) {
            this.loginApp = loginApp;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getTopUpBalance() {
            return topUpBalance;
        }

        public void setTopUpBalance(String topUpBalance) {
            this.topUpBalance = topUpBalance;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }
}
