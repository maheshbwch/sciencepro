package com.example.sciencepro.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RegisterUserResponse extends BaseModel {

    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("employee_id")
        @Expose
        private String employeeId;
        @SerializedName("office_shift_id")
        @Expose
        private String officeShiftId;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("date_of_birth")
        @Expose
        private String dateOfBirth;
        @SerializedName("country_id")
        @Expose
        private String countryId;
        @SerializedName("app_id")
        @Expose
        private String appId;
        @SerializedName("ic")
        @Expose
        private String ic;
        @SerializedName("university")
        @Expose
        private String university;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("race")
        @Expose
        private String race;
        @SerializedName("e_status")
        @Expose
        private String eStatus;
        @SerializedName("user_role_id")
        @Expose
        private String userRoleId;
        @SerializedName("department_id")
        @Expose
        private String departmentId;
        @SerializedName("sub_department_id")
        @Expose
        private String subDepartmentId;
        @SerializedName("designation_id")
        @Expose
        private String designationId;
        @SerializedName("company_id")
        @Expose
        private String locationId;
        @SerializedName("view_companies_id")
        @Expose
        private String viewCompaniesId;
        @SerializedName("salary_template")
        @Expose
        private String salaryTemplate;
        @SerializedName("hourly_grade_id")
        @Expose
        private String hourlyGradeId;
        @SerializedName("monthly_grade_id")
        @Expose
        private String monthlyGradeId;
        @SerializedName("date_of_joining")
        @Expose
        private String dateOfJoining;
        @SerializedName("date_of_leaving")
        @Expose
        private String dateOfLeaving;
        @SerializedName("marital_status")
        @Expose
        private String maritalStatus;
        @SerializedName("salary")
        @Expose
        private String salary;
        @SerializedName("wages_type")
        @Expose
        private String wagesType;
        @SerializedName("basic_salary")
        @Expose
        private String basicSalary;
        @SerializedName("daily_wages")
        @Expose
        private String dailyWages;
        @SerializedName("salary_ssempee")
        @Expose
        private String salarySsempee;
        @SerializedName("salary_ssempeer")
        @Expose
        private String salarySsempeer;
        @SerializedName("salary_income_tax")
        @Expose
        private String salaryIncomeTax;
        @SerializedName("salary_overtime")
        @Expose
        private String salaryOvertime;
        @SerializedName("salary_commission")
        @Expose
        private String salaryCommission;
        @SerializedName("salary_claims")
        @Expose
        private String salaryClaims;
        @SerializedName("salary_paid_leave")
        @Expose
        private String salaryPaidLeave;
        @SerializedName("salary_director_fees")
        @Expose
        private String salaryDirectorFees;
        @SerializedName("salary_bonus")
        @Expose
        private String salaryBonus;
        @SerializedName("salary_advance_paid")
        @Expose
        private String salaryAdvancePaid;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("emergency_contact")
        @Expose
        private String emergencyContact;
        @SerializedName("half_body_image")
        @Expose
        private String halfBodyImage;
        @SerializedName("identity_card_type")
        @Expose
        private String identityCardType;
        @SerializedName("identity_image")
        @Expose
        private String identityImage;
        @SerializedName("passport_image")
        @Expose
        private String passportImage;
        @SerializedName("visa_image")
        @Expose
        private String visaImage;
        @SerializedName("zipcode")
        @Expose
        private String zipcode;
        @SerializedName("profile_picture")
        @Expose
        private String profilePicture;
        @SerializedName("profile_background")
        @Expose
        private String profileBackground;
        @SerializedName("resume")
        @Expose
        private String resume;
        @SerializedName("skype_id")
        @Expose
        private String skypeId;
        @SerializedName("contact_no")
        @Expose
        private String contactNo;
        @SerializedName("facebook_link")
        @Expose
        private String facebookLink;
        @SerializedName("twitter_link")
        @Expose
        private String twitterLink;
        @SerializedName("blogger_link")
        @Expose
        private String bloggerLink;
        @SerializedName("linkdedin_link")
        @Expose
        private String linkdedinLink;
        @SerializedName("google_plus_link")
        @Expose
        private String googlePlusLink;
        @SerializedName("instagram_link")
        @Expose
        private String instagramLink;
        @SerializedName("pinterest_link")
        @Expose
        private String pinterestLink;
        @SerializedName("youtube_link")
        @Expose
        private String youtubeLink;
        @SerializedName("is_active")
        @Expose
        private String isActive;
        @SerializedName("last_login_date")
        @Expose
        private String lastLoginDate;
        @SerializedName("last_logout_date")
        @Expose
        private String lastLogoutDate;
        @SerializedName("last_login_ip")
        @Expose
        private String lastLoginIp;
        @SerializedName("is_logged_in")
        @Expose
        private String isLoggedIn;
        @SerializedName("online_status")
        @Expose
        private String onlineStatus;
        @SerializedName("fixed_header")
        @Expose
        private String fixedHeader;
        @SerializedName("compact_sidebar")
        @Expose
        private String compactSidebar;
        @SerializedName("boxed_wrapper")
        @Expose
        private String boxedWrapper;
        @SerializedName("leave_categories")
        @Expose
        private String leaveCategories;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("latitude")
        private String isApproved;
        @SerializedName("fcm_token")
        @Expose
        private String _new;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getOfficeShiftId() {
            return officeShiftId;
        }

        public void setOfficeShiftId(String officeShiftId) {
            this.officeShiftId = officeShiftId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getIc() {
            return ic;
        }

        public void setIc(String ic) {
            this.ic = ic;
        }

        public String getUniversity() {
            return university;
        }

        public void setUniversity(String university) {
            this.university = university;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getRace() {
            return race;
        }

        public void setRace(String race) {
            this.race = race;
        }

        public String getEStatus() {
            return eStatus;
        }

        public void setEStatus(String eStatus) {
            this.eStatus = eStatus;
        }

        public String getUserRoleId() {
            return userRoleId;
        }

        public void setUserRoleId(String userRoleId) {
            this.userRoleId = userRoleId;
        }

        public String getDepartmentId() {
            return departmentId;
        }

        public void setDepartmentId(String departmentId) {
            this.departmentId = departmentId;
        }

        public String getSubDepartmentId() {
            return subDepartmentId;
        }

        public void setSubDepartmentId(String subDepartmentId) {
            this.subDepartmentId = subDepartmentId;
        }

        public String getDesignationId() {
            return designationId;
        }

        public void setDesignationId(String designationId) {
            this.designationId = designationId;
        }


        public String getLocationId() {
            return locationId;
        }

        public void setLocationId(String locationId) {
            this.locationId = locationId;
        }

        public String getViewCompaniesId() {
            return viewCompaniesId;
        }

        public void setViewCompaniesId(String viewCompaniesId) {
            this.viewCompaniesId = viewCompaniesId;
        }

        public String getSalaryTemplate() {
            return salaryTemplate;
        }

        public void setSalaryTemplate(String salaryTemplate) {
            this.salaryTemplate = salaryTemplate;
        }

        public String getHourlyGradeId() {
            return hourlyGradeId;
        }

        public void setHourlyGradeId(String hourlyGradeId) {
            this.hourlyGradeId = hourlyGradeId;
        }

        public String getMonthlyGradeId() {
            return monthlyGradeId;
        }

        public void setMonthlyGradeId(String monthlyGradeId) {
            this.monthlyGradeId = monthlyGradeId;
        }

        public String getDateOfJoining() {
            return dateOfJoining;
        }

        public void setDateOfJoining(String dateOfJoining) {
            this.dateOfJoining = dateOfJoining;
        }

        public String getDateOfLeaving() {
            return dateOfLeaving;
        }

        public void setDateOfLeaving(String dateOfLeaving) {
            this.dateOfLeaving = dateOfLeaving;
        }

        public String getMaritalStatus() {
            return maritalStatus;
        }

        public void setMaritalStatus(String maritalStatus) {
            this.maritalStatus = maritalStatus;
        }

        public String getSalary() {
            return salary;
        }

        public void setSalary(String salary) {
            this.salary = salary;
        }

        public String getWagesType() {
            return wagesType;
        }

        public void setWagesType(String wagesType) {
            this.wagesType = wagesType;
        }

        public String getBasicSalary() {
            return basicSalary;
        }

        public void setBasicSalary(String basicSalary) {
            this.basicSalary = basicSalary;
        }

        public String getDailyWages() {
            return dailyWages;
        }

        public void setDailyWages(String dailyWages) {
            this.dailyWages = dailyWages;
        }

        public String getSalarySsempee() {
            return salarySsempee;
        }

        public void setSalarySsempee(String salarySsempee) {
            this.salarySsempee = salarySsempee;
        }

        public String getSalarySsempeer() {
            return salarySsempeer;
        }

        public void setSalarySsempeer(String salarySsempeer) {
            this.salarySsempeer = salarySsempeer;
        }

        public String getSalaryIncomeTax() {
            return salaryIncomeTax;
        }

        public void setSalaryIncomeTax(String salaryIncomeTax) {
            this.salaryIncomeTax = salaryIncomeTax;
        }

        public String getSalaryOvertime() {
            return salaryOvertime;
        }

        public void setSalaryOvertime(String salaryOvertime) {
            this.salaryOvertime = salaryOvertime;
        }

        public String getSalaryCommission() {
            return salaryCommission;
        }

        public void setSalaryCommission(String salaryCommission) {
            this.salaryCommission = salaryCommission;
        }

        public String getSalaryClaims() {
            return salaryClaims;
        }

        public void setSalaryClaims(String salaryClaims) {
            this.salaryClaims = salaryClaims;
        }

        public String getSalaryPaidLeave() {
            return salaryPaidLeave;
        }

        public void setSalaryPaidLeave(String salaryPaidLeave) {
            this.salaryPaidLeave = salaryPaidLeave;
        }

        public String getSalaryDirectorFees() {
            return salaryDirectorFees;
        }

        public void setSalaryDirectorFees(String salaryDirectorFees) {
            this.salaryDirectorFees = salaryDirectorFees;
        }

        public String getSalaryBonus() {
            return salaryBonus;
        }

        public void setSalaryBonus(String salaryBonus) {
            this.salaryBonus = salaryBonus;
        }

        public String getSalaryAdvancePaid() {
            return salaryAdvancePaid;
        }

        public void setSalaryAdvancePaid(String salaryAdvancePaid) {
            this.salaryAdvancePaid = salaryAdvancePaid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getEmergencyContact() {
            return emergencyContact;
        }

        public void setEmergencyContact(String emergencyContact) {
            this.emergencyContact = emergencyContact;
        }

        public String getHalfBodyImage() {
            return halfBodyImage;
        }

        public void setHalfBodyImage(String halfBodyImage) {
            this.halfBodyImage = halfBodyImage;
        }

        public String getIdentityCardType() {
            return identityCardType;
        }

        public void setIdentityCardType(String identityCardType) {
            this.identityCardType = identityCardType;
        }

        public String getIdentityImage() {
            return identityImage;
        }

        public void setIdentityImage(String identityImage) {
            this.identityImage = identityImage;
        }

        public String getPassportImage() {
            return passportImage;
        }

        public void setPassportImage(String passportImage) {
            this.passportImage = passportImage;
        }

        public String getVisaImage() {
            return visaImage;
        }

        public void setVisaImage(String visaImage) {
            this.visaImage = visaImage;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getProfileBackground() {
            return profileBackground;
        }

        public void setProfileBackground(String profileBackground) {
            this.profileBackground = profileBackground;
        }

        public String getResume() {
            return resume;
        }

        public void setResume(String resume) {
            this.resume = resume;
        }

        public String getSkypeId() {
            return skypeId;
        }

        public void setSkypeId(String skypeId) {
            this.skypeId = skypeId;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getFacebookLink() {
            return facebookLink;
        }

        public void setFacebookLink(String facebookLink) {
            this.facebookLink = facebookLink;
        }

        public String getTwitterLink() {
            return twitterLink;
        }

        public void setTwitterLink(String twitterLink) {
            this.twitterLink = twitterLink;
        }

        public String getBloggerLink() {
            return bloggerLink;
        }

        public void setBloggerLink(String bloggerLink) {
            this.bloggerLink = bloggerLink;
        }

        public String getLinkdedinLink() {
            return linkdedinLink;
        }

        public void setLinkdedinLink(String linkdedinLink) {
            this.linkdedinLink = linkdedinLink;
        }

        public String getGooglePlusLink() {
            return googlePlusLink;
        }

        public void setGooglePlusLink(String googlePlusLink) {
            this.googlePlusLink = googlePlusLink;
        }

        public String getInstagramLink() {
            return instagramLink;
        }

        public void setInstagramLink(String instagramLink) {
            this.instagramLink = instagramLink;
        }

        public String getPinterestLink() {
            return pinterestLink;
        }

        public void setPinterestLink(String pinterestLink) {
            this.pinterestLink = pinterestLink;
        }

        public String getYoutubeLink() {
            return youtubeLink;
        }

        public void setYoutubeLink(String youtubeLink) {
            this.youtubeLink = youtubeLink;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getLastLoginDate() {
            return lastLoginDate;
        }

        public void setLastLoginDate(String lastLoginDate) {
            this.lastLoginDate = lastLoginDate;
        }

        public String getLastLogoutDate() {
            return lastLogoutDate;
        }

        public void setLastLogoutDate(String lastLogoutDate) {
            this.lastLogoutDate = lastLogoutDate;
        }

        public String getLastLoginIp() {
            return lastLoginIp;
        }

        public void setLastLoginIp(String lastLoginIp) {
            this.lastLoginIp = lastLoginIp;
        }

        public String getIsLoggedIn() {
            return isLoggedIn;
        }

        public void setIsLoggedIn(String isLoggedIn) {
            this.isLoggedIn = isLoggedIn;
        }

        public String getOnlineStatus() {
            return onlineStatus;
        }

        public void setOnlineStatus(String onlineStatus) {
            this.onlineStatus = onlineStatus;
        }

        public String getFixedHeader() {
            return fixedHeader;
        }

        public void setFixedHeader(String fixedHeader) {
            this.fixedHeader = fixedHeader;
        }

        public String getCompactSidebar() {
            return compactSidebar;
        }

        public void setCompactSidebar(String compactSidebar) {
            this.compactSidebar = compactSidebar;
        }

        public String getBoxedWrapper() {
            return boxedWrapper;
        }

        public void setBoxedWrapper(String boxedWrapper) {
            this.boxedWrapper = boxedWrapper;
        }

        public String getLeaveCategories() {
            return leaveCategories;
        }

        public void setLeaveCategories(String leaveCategories) {
            this.leaveCategories = leaveCategories;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }


        public String getIsApproved() {
            return isApproved;
        }

        public void setIsApproved(String isApproved) {
            this.isApproved = isApproved;
        }


        public String getNew() {
            return _new;
        }

        public void setNew(String _new) {
            this._new = _new;
        }

    }
}
