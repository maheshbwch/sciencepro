package com.example.sciencepro.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ReadAnswerModel extends BaseModel {

    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("question_id")
        @Expose
        private String questionId;
        @SerializedName("questioner_name")
        @Expose
        private Object questionerName;
        @SerializedName("profile_picture")
        @Expose
        private String profilePicture;
        @SerializedName("app_name")
        @Expose
        private String appName;
        @SerializedName("chapter_name")
        @Expose
        private String chapterName;
        @SerializedName("topic_name")
        @Expose
        private String topicName;
        @SerializedName("topic_details")
        @Expose
        private String topicDetails;
        @SerializedName("question_details")
        @Expose
        private String questionDetails;
        @SerializedName("quotation")
        @Expose
        private String quotation;
        @SerializedName("answer_details")
        @Expose
        private String answerDetails;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("time_taken")
        @Expose
        private String timeTaken;
        @SerializedName("attachment_file")
        @Expose
        private List<AttachmentFile> attachmentFile = null;

        @SerializedName("question_attachment_file")
        @Expose
        private List<QuestionsAttachmentFile> questionAttachmentFile = null;

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("time")
        @Expose
        private String time;

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public Object getQuestionerName() {
            return questionerName;
        }

        public void setQuestionerName(Object questionerName) {
            this.questionerName = questionerName;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getQuestionDetails() {
            return questionDetails;
        }

        public void setQuestionDetails(String questionDetails) {
            this.questionDetails = questionDetails;
        }

        public String getQuotation() {
            return quotation;
        }

        public void setQuotation(String quotation) {
            this.quotation = quotation;
        }

        public String getAnswerDetails() {
            return answerDetails;
        }

        public void setAnswerDetails(String answerDetails) {
            this.answerDetails = answerDetails;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTimeTaken() {
            return timeTaken;
        }

        public void setTimeTaken(String timeTaken) {
            this.timeTaken = timeTaken;
        }

        public List<AttachmentFile> getAttachmentFile() {
            return attachmentFile;
        }

        public void setAttachmentFile(List<AttachmentFile> attachmentFile) {
            this.attachmentFile = attachmentFile;
        }

        public List<QuestionsAttachmentFile> getQuestionAttachmentFile() {
            return questionAttachmentFile;
        }

        public void setQuestionAttachmentFile(List<QuestionsAttachmentFile> questionAttachmentFile) {
            this.questionAttachmentFile = questionAttachmentFile;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

    public class AttachmentFile {

        @SerializedName("file")
        @Expose
        private String file;
        @SerializedName("download_file")
        @Expose
        private String downloadFile;


        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getDownloadFile() {
            return downloadFile;
        }

        public void setDownloadFile(String downloadFile) {
            this.downloadFile = downloadFile;
        }

    }

    public class QuestionsAttachmentFile {

        @SerializedName("file")
        @Expose
        private String file;

        @SerializedName("download_file")
        @Expose
        private String downloadFile;

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getDownloadFile() {
            return downloadFile;
        }

        public void setDownloadFile(String downloadFile) {
            this.downloadFile = downloadFile;
        }

    }
}
