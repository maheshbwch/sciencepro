package com.example.sciencepro.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RecentQuestionsModel extends BaseModel {

    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    @SerializedName("top_up_balance")
    @Expose
    private String topUpBalance;

    public String getTopUpBalance() {
        return topUpBalance;
    }

    public void setTopUpBalance(String topUpBalance) {
        this.topUpBalance = topUpBalance;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("question_id")
        @Expose
        private String questionId;
        @SerializedName("chapter_name")
        @Expose
        private String chapterName;
        @SerializedName("chapter_id")
        @Expose
        private String chapterId;
        @SerializedName("topic_name")
        @Expose
        private String topicName;
        @SerializedName("topic_id")
        @Expose
        private String topicId;
        @SerializedName("topic_details")
        @Expose
        private String topicDetails;
        @SerializedName("quotation")
        @Expose
        private String quotation;
        @SerializedName("question_details")
        @Expose
        private String questionDetails;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("is_urgent")
        @Expose
        private String isUrgent;

        @SerializedName("time_taken")
        @Expose
        private String timeTaken;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterId() {
            return chapterId;
        }

        public void setChapterId(String chapterId) {
            this.chapterId = chapterId;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }

        public String getTopicId() {
            return topicId;
        }

        public void setTopicId(String topicId) {
            this.topicId = topicId;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getQuotation() {
            return quotation;
        }

        public void setQuotation(String quotation) {
            this.quotation = quotation;
        }

        public String getQuestionDetails() {
            return questionDetails;
        }

        public void setQuestionDetails(String questionDetails) {
            this.questionDetails = questionDetails;
        }


        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIsUrgent() {
            return isUrgent;
        }

        public void setIsUrgent(String isUrgent) {
            this.isUrgent = isUrgent;
        }

        public String getTimeTaken() {
            return timeTaken;
        }

        public void setTimeTaken(String timeTaken) {
            this.timeTaken = timeTaken;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }
}
