package com.example.sciencepro.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.example.sciencepro.R;
import com.example.sciencepro.adapters.HomePagerAdapter;

public class HomeActivity extends AppCompatActivity {

    private TextView titleTxt;
    private ViewPager viewPager;
    private BottomNavigationView bottomNavigation;

    private HomePagerAdapter homePagerAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        titleTxt = findViewById(R.id.ah_titleTxt);
        viewPager = findViewById(R.id.ah_viewPager);
        bottomNavigation = findViewById(R.id.ah_bottomNavigation);

        init();
    }

    private void init() {


        listeners();

    }

    private void listeners() {

        homePagerAdapter = new HomePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(homePagerAdapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.e("selected", "page:" + position);
                setUpToolBarTitle(position);
                setNavigationSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.ahm_home_item:
                        setUpToolBarTitle(0);
                        setNavigationSelected(0);
                        showPagerFragment(0);
                        break;
                    case R.id.ahm_notification_item:
                        setUpToolBarTitle(1);
                        setNavigationSelected(1);
                        showPagerFragment(1);
                        break;
                    case R.id.ahm_jobs_item:
                        setUpToolBarTitle(2);
                        setNavigationSelected(2);
                        showPagerFragment(2);
                        break;
                    case R.id.ahm_chat_item:
                        setUpToolBarTitle(3);
                        setNavigationSelected(3);
                        showPagerFragment(3);
                        break;

                    case R.id.ahm_account_item:
                        setUpToolBarTitle(4);
                        setNavigationSelected(4);
                        showPagerFragment(4);
                        break;
                    default:
                        break;
                }
                Log.e("menu", "item:" + menuItem.getItemId());
                return false;
            }
        });

    }

    private void setUpToolBarTitle(int position) {
        switch (position) {
            case 0:
                titleTxt.setText("Home");
                //imgFilter.setVisibility(View.GONE);
                break;
            case 1:
                titleTxt.setText("FAQs");
                //imgFilter.setVisibility(View.GONE);
                break;
            case 2:
                titleTxt.setText("History");
                //imgFilter.setVisibility(View.VISIBLE);
                break;
            case 3:
                titleTxt.setText("Notification");
                //imgFilter.setVisibility(View.GONE);
                break;
            case 4:
                titleTxt.setText("Profile");
                //imgFilter.setVisibility(View.GONE);
                break;
            default:
                break;

        }
    }


    private void setNavigationSelected(int position) {
        bottomNavigation.getMenu().getItem(position).setChecked(true);
    }

    private void showPagerFragment(int position) {
        viewPager.setCurrentItem(position);
    }
}
