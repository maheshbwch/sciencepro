package com.example.sciencepro.activities.accounts;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sciencepro.R;
import com.example.sciencepro.models.BaseModel;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.MyUtils;
import com.example.sciencepro.utils.PreferenceHandler;
import com.example.sciencepro.webservice.ApiInterface;
import com.example.sciencepro.webservice.HttpRequest;
import com.google.android.material.textfield.TextInputEditText;;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {

    private ImageView imgBack;
    private ProgressDialog progressDialog = null;
    private Context context;
    private TextView txtSave;
    private TextInputEditText edtCurrentPass, edtNewPass, edtConfirmPass;
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        imgBack = findViewById(R.id.imgBack);
        context = ChangePassword.this;

        txtSave = findViewById(R.id.acp_txtSave);
        edtCurrentPass = findViewById(R.id.acp_edtCurrentPass);
        edtNewPass = findViewById(R.id.acp_edtNewPass);
        edtConfirmPass = findViewById(R.id.acp_edtConfirmPass);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        txtSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields(v);

            }
        });
    }

    private void validateFields(View v) {

        String currentPassword = edtCurrentPass.getText().toString();
        String newPassword = edtNewPass.getText().toString();
        String confirmPassword = edtConfirmPass.getText().toString();

        if (!MyUtils.checkStringValue(currentPassword)) {
            MyUtils.showLongToast(context, "Current Password required");
        } else if (!MyUtils.checkStringValue(newPassword)) {
            MyUtils.showLongToast(context, "New Password required");
        } else if (!MyUtils.checkStringValue(confirmPassword)) {
            MyUtils.showLongToast(context, "Confirm New Password required");
        } else {
            if (newPassword.equals(confirmPassword)) {
                changePassword(currentPassword, newPassword, v);
            } else {
                MyUtils.showLongToast(context, "New Password and Confirm New Password are incorrect");
            }
        }

    }

    private void changePassword(String currentPass, String newPassword, View v) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.changePassword(Constants.API_KEY, Constants.PLATFORM, userId, currentPass, newPassword);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        BaseModel baseModel = response.body();
                        if (baseModel != null) {
                            String status = String.valueOf(baseModel.getStatus());
                            String message = baseModel.getMessage();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(String.valueOf(Constants.SUCCESS))) {
                                //showAlertToUser(Constants.DIALOG_SUCCESS, "Update Password", MyUtils.checkStringValue(message) ? message : "User Password Updated Successfully");
                                MyUtils.showLongToast(context, "Your Password Updated Successfully");
                                finish();
                            } else {
                                //showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", MyUtils.checkStringValue(message) ? message : "Unable to Update User Password");
                                MyUtils.showSnackBar(v, context, MyUtils.checkStringValue(message) ? message : "Unable to Update User Password");
                            }
                        } else {
                            Log.e("error", "null response");
                            //showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", "Unable to Update User Password");
                            MyUtils.showSnackBar(v, context, "Unable to Update User Password");

                        }
                    } else {
                        Log.e("error", "response un successful");
                        //showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", "Unable to Update User Password");
                        MyUtils.showSnackBar(v, context, "Unable to Update User Password");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    //showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", "Unable to Update User Password");
                    MyUtils.showSnackBar(v, context, "Unable to Update User Password");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            //showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", "Unable to Update User Password");
            MyUtils.showSnackBar(v, context, "Unable to Update User Password");
        }
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, ChangePassword.this);
    }
}
