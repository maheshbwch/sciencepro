package com.example.sciencepro.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.example.sciencepro.R;
import com.example.sciencepro.activities.register.RegistrationPhoneActivity;
import com.example.sciencepro.models.LoginResponse;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.MyUtils;
import com.example.sciencepro.utils.PreferenceHandler;
import com.example.sciencepro.webservice.ApiInterface;
import com.example.sciencepro.webservice.HttpRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgFull;
    private TextView txtLogin;
    private LinearLayout linearRegister;
    private TextInputEditText edtNumber, edtPassword;
    private ProgressDialog progressDialog = null;
    private String fcmToken="",deviceId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;

        imgFull = findViewById(R.id.imgFull);
        txtLogin = findViewById(R.id.al_txtLogin);
        linearRegister = findViewById(R.id.registerLinear);

        edtNumber = findViewById(R.id.al_edtNumber);
        edtPassword = findViewById(R.id.al_edtPassword);

        Glide.with(LoginActivity.this).load(R.drawable.background_login).into(imgFull);

        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFeilds(v);
            }
        });

        getFcmToken();

        linearRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, RegistrationPhoneActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, LoginActivity.this);
            }
        });

    }

    private void validateFeilds(View v) {

        if (MyUtils.isInternetConnected(context)) {
            String phone = edtNumber.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();

            if ((phone.length() > 4) && (MyUtils.checkStringValue(phone))) {

                if (!MyUtils.checkStringValue(password)) {
                    MyUtils.showSnackBar(v, context, "Password Required");
                } else {
                    loginApiCall(phone, password, v);
                }
            } else {
                MyUtils.showSnackBar(v, context, "Please enter valid phone number");
            }

        } else {
            MyUtils.showSnackBar(v, context, String.valueOf(R.string.internet_failed));
        }

    }

    private void loginApiCall(String phone, String password, View v) {


        try {
            progressDialog = MyUtils.showProgressLoader(context, "Please Wait..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<LoginResponse> call = apiService.loginUser(Constants.API_KEY, phone, password, fcmToken, deviceId, Constants.APP_ID);//TODO CHANGE FCM
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        LoginResponse userLoginResponse = response.body();

                        if (userLoginResponse != null) {
                            String status = userLoginResponse.getStatus();
                            String error = userLoginResponse.getMessage();
                            if (MyUtils.checkStringValue(status)) {
                                if (status.equalsIgnoreCase(Constants.SUCCESS)) {

                                    String userId = userLoginResponse.getData().get(0).getUserId();
                                    String creditBalance = userLoginResponse.getData().get(0).getTopUpBalance();
                                    PreferenceHandler.storePreference(context, Constants.USER_ID, userId);
                                    PreferenceHandler.storePreference(context, Constants.USER_LOGIN, Constants.USER_LOGIN);

                                    MyUtils.showLongToast(context, "Welcome " + userLoginResponse.getData().get(0).getName());
                                    intentToHome();
                                } else if (status.equalsIgnoreCase(Constants.FAILURE)) {
                                    MyUtils.showSnackBar(v, context, MyUtils.checkStringValue(error) ? error : "Invalid username or password");
                                }
                            }
                        } else {
                            Log.e("error", "null response");
                            MyUtils.showSnackBar(v, context, "Unable to Validate Your Number");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        MyUtils.showSnackBar(v, context, "Unable to Validate Your Number");
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    MyUtils.showSnackBar(v, context, "Unable to Validate Your Number");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            MyUtils.showSnackBar(v, context, "Unable to Validate Your Number");
        }
    }

    private void intentToHome() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, LoginActivity.this);
        finish();
    }

    private void getFcmToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w("notifications", "getInstanceId failed", task.getException());
                }
                 fcmToken = task.getResult().getToken();
                Log.e("fcm_token", "token: " + fcmToken);
            }
        });

         deviceId = MyUtils.getDeviceId(LoginActivity.this);
        Log.e("deviceId", "deviceId: " + deviceId);
    }


}
