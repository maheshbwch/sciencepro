package com.example.sciencepro.activities.register;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.example.sciencepro.R;
import com.example.sciencepro.dialogs.CountryListDialog;
import com.example.sciencepro.interfaces.DialogInterface;
import com.example.sciencepro.interfaces.OnSelectedListener;
import com.example.sciencepro.models.BaseModel;
import com.example.sciencepro.models.CountryCodes;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.CustomDialog;
import com.example.sciencepro.utils.MyUtils;
import com.example.sciencepro.webservice.ApiInterface;
import com.example.sciencepro.webservice.HttpRequest;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationPhoneActivity extends AppCompatActivity {

    private Context context;
    private LinearLayout linearNext, linearTxtCountryCode;
    private ImageView imgBack;
    private TextView txtCountry;
    private TextInputEditText edtPhoneNumber, edtPassword;

    private ArrayList<CountryCodes.Datum> countryCodesList = new ArrayList<>();
    private CountryListDialog countryListDialog = null;
    private ProgressDialog progressDialog = null;

    private String countryISDCode = null;
    private String countryCodeId = null;

    private String phoneNumberWithCC = "";
    private String phoneNumber = "", passWord = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        context = RegistrationPhoneActivity.this;

        linearTxtCountryCode = findViewById(R.id.linearCountryCode);
        linearNext = findViewById(R.id.nextRegisterAccount);
        imgBack = findViewById(R.id.imgBack);
        txtCountry = findViewById(R.id.ar_countryCode);
        edtPhoneNumber = findViewById(R.id.ar_edtNumber);
        edtPassword = findViewById(R.id.ar_edtPassword);

        countryISDCode = Constants.MALAYSIA_COUNTRY_CODE;

        linearNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                phoneNumber = edtPhoneNumber.getText().toString().trim();
                passWord = edtPassword.getText().toString().trim();

                //For default selection malasiyacode
                phoneNumberWithCC = countryISDCode + phoneNumber;

                if (!MyUtils.checkStringValue(phoneNumber)) {
                    MyUtils.showSnackBar(v, context, "Phone Number field is required");
                } else if (!(phoneNumber.length() > 5)) {
                    MyUtils.showSnackBar(v, context, "The entered number is invalid");
                } else if (!MyUtils.checkStringValue(passWord)) {
                    MyUtils.showSnackBar(v, context, "Password field is required");
                } else {
                    if (MyUtils.isConnected(context)) {
                        userPhoneValidation(v);
                    } else {
                        MyUtils.showSnackBar(v, context, String.valueOf(R.string.internet_failed));
                    }
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        if (MyUtils.isConnected(context)) {
            getCountryCodeList();
        } else {
            MyUtils.showLongToast(context, String.valueOf(R.string.internet_failed));
        }

    }

    private void userPhoneValidation(View v) {

        try {
            progressDialog = MyUtils.showProgressLoader(context, "Validating...");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.userValidation(Constants.API_KEY, phoneNumber, Constants.APP_ID, countryCodeId, Constants.USER_ROLE_ID);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        BaseModel countryCodes = response.body();
                        if (countryCodes != null) {
                            String status = countryCodes.getStatus();
                            String message = countryCodes.getMessage();
                            if (MyUtils.checkStringValue(status)) {
                                if (status.equalsIgnoreCase(Constants.SUCCESS)) {
                                    showAlertToUser(Constants.DIALOG_GENERAL, "Verify Phone Number " + phoneNumberWithCC, "OTP message will be sent to given Phone Number", true);
                                } else if (status.equalsIgnoreCase(Constants.FAILURE)) {
                                    MyUtils.showSnackBar(v, context, MyUtils.checkStringValue(message) ? message : "This phone number is already exists");
                                }
                            } else {
                                Log.e("error", "null response");
                                MyUtils.showSnackBar(v, context, "Unable to verify your number");
                            }

                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
        }

    }

    private void getCountryCodeList() {
        try {
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<CountryCodes> call = apiService.getCountriesCodesList(Constants.API_KEY);
            call.enqueue(new Callback<CountryCodes>() {
                @Override
                public void onResponse(Call<CountryCodes> call, Response<CountryCodes> response) {
                    if (response.isSuccessful()) {

                        CountryCodes countryCodes = response.body();

                        if (countryCodes != null) {

                            updateCountryCodeToDialogUI(countryCodes);

                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<CountryCodes> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void updateCountryCodeToDialogUI(CountryCodes countryCodes) {
        countryCodeId = Constants.MALAYSIA_COUNTRY_CODE_ID_API;
        countryCodesList.addAll(countryCodes.getData());
        Log.e("countryCodesList", "size:" + countryCodesList.size());
        if (countryCodesList.size() > 0) {
            linearTxtCountryCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (countryListDialog == null) {
                        countryListDialog = new CountryListDialog(countryCodesList, new OnSelectedListener() {
                            @Override
                            public void onSelected(CountryCodes.Datum countryCodes) {

                                //String flagUrl = countryCodes.getUrlPath();
                                //String countryName = countryCodes.getCountriesName();
                                String countryCode = countryCodes.getCountriesIsdCode();
                                countryCodeId = countryCodes.getCountriesId();
                                countryISDCode = "+" + countryCode;
                                txtCountry.setText("" + countryISDCode);
                                countryISDCode = countryISDCode + " ";
                                phoneNumberWithCC = countryISDCode + phoneNumber;
                            }

                            @Override
                            public void onNothingSelected() {

                            }
                        });
                        countryListDialog.setTitle("Select Your Country");
                    }
                    countryListDialog.show(getSupportFragmentManager(), "dialog");
                }
            });
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage, final boolean state) {
        CustomDialog customDialog = new CustomDialog(RegistrationPhoneActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                if (state) {
                    if (Constants.SKIP_OTP) {
                        intentToOtpVerifyPage("123456");
                    } else {
                        sendVerificationCode(phoneNumberWithCC);
                    }
                }
            }

            @Override
            public void onDismissedClicked() {
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }

    private void sendVerificationCode(String mobile) {
        progressDialog = MyUtils.showProgressLoader(context, "Sending Code...");
        PhoneAuthProvider provider = PhoneAuthProvider.getInstance();
        provider.verifyPhoneNumber(mobile, Constants.OTP_TIME_OUT, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD, mCallbacks);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.e("verification", "code:" + code);
            MyUtils.dismissProgressLoader(progressDialog);

            Toast.makeText(RegistrationPhoneActivity.this, "code:" + code, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The phone number that you have entered is invalid", false);
            } else if (e instanceof FirebaseTooManyRequestsException) {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The entered phone number : " + phoneNumberWithCC + " is attempted too many OTP verification. Please try after some time", false);
            } else {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The entered phone number : " + phoneNumberWithCC + " is verification failed. Please try again", false);
            }
            Log.e("verification", "exp:" + e.getMessage());
            MyUtils.dismissProgressLoader(progressDialog);
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            Log.e("verification", "id:" + verificationId);
            MyUtils.dismissProgressLoader(progressDialog);
            intentToOtpVerifyPage(verificationId);

        }
    };


    private void intentToOtpVerifyPage(String verificationId) {
        Intent intent = new Intent(RegistrationPhoneActivity.this, VerificationActivity.class);
        intent.putExtra(Constants.VERIFICATION_ID, verificationId);
        intent.putExtra(Constants.COUNTRY_CODE_ID, countryCodeId);
        intent.putExtra(Constants.PHONE_NUMBER, phoneNumber);
        intent.putExtra(Constants.PASSWORD, passWord);
        intent.putExtra(Constants.PHONE_WITH_CC, phoneNumberWithCC);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, RegistrationPhoneActivity.this);
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, RegistrationPhoneActivity.this);
    }

}
