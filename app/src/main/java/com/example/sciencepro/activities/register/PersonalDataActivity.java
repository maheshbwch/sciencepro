package com.example.sciencepro.activities.register;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.example.sciencepro.R;
import com.example.sciencepro.activities.HomeActivity;
import com.example.sciencepro.interfaces.DialogInterface;
import com.example.sciencepro.interfaces.UploadAlertListener;
import com.example.sciencepro.models.BaseModel;
import com.example.sciencepro.models.RegisterUserResponse;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.CustomDialog;
import com.example.sciencepro.utils.MyUtils;
import com.example.sciencepro.utils.PermissionAlert;
import com.example.sciencepro.utils.PermissionRequest;
import com.example.sciencepro.utils.PreferenceHandler;
import com.example.sciencepro.utils.RealPathUtil;
import com.example.sciencepro.utils.UploadAlert;
import com.example.sciencepro.webservice.ApiInterface;
import com.example.sciencepro.webservice.HttpRequest;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalDataActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private Context context;
    private LinearLayout nextLinear, saveLinear;
    private ImageView imgBack, imgPhoto, imgCamera;
    private TextInputEditText edtUserName, edtIc, edtEmail, edtUniversity;
    private RelativeLayout relativePhoto;
    private ProgressDialog progressDialog = null;
    private RadioButton radioMale, radioFemale;
    private String imageFilePath = "", gender = "";
    private String isFrom = "";

    private String phoneNumber = "", countryId = "", password = "";

    private String imgUrl = "", userName = "", ic = "", email = "", university = "";
    private String userId = "";
    private String fcmToken = "", deviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_data);

        init();

        getFcmToken();

    }


    private void init() {

        ((RadioGroup) findViewById(R.id.fancy_radio_group)).setOnCheckedChangeListener(this);

        context = PersonalDataActivity.this;
        nextLinear = findViewById(R.id.nextRegisterPersonal);
        saveLinear = findViewById(R.id.saveRegisterPersonal);
        imgBack = findViewById(R.id.imgBack);
        imgPhoto = findViewById(R.id.apd_profileImageView);
        imgCamera = findViewById(R.id.apd_cameraIcon);
        relativePhoto = findViewById(R.id.apd_profileImageRelative);
        radioMale = findViewById(R.id.radioMale);
        radioFemale = findViewById(R.id.radioFemale);

        edtUserName = findViewById(R.id.apd_nameEdt);
        edtIc = findViewById(R.id.apd_edtIc);
        edtEmail = findViewById(R.id.apd_edtEmail);
        edtUniversity = findViewById(R.id.apd_edtUniversity);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        isFrom = getIntent().getStringExtra(Constants.IS_FROM);

        if (isFrom.equalsIgnoreCase(Constants.IS_FROM_REGISTER)) {
            phoneNumber = getIntent().getStringExtra(Constants.PHONE_NUMBER);
            password = getIntent().getStringExtra(Constants.PASSWORD);
            countryId = getIntent().getStringExtra(Constants.COUNTRY_CODE_ID);
        } else if (isFrom.equalsIgnoreCase(Constants.IS_FROM_ACCOUNT_EDIT)) {
            imgUrl = getIntent().getStringExtra("image");
            userName = getIntent().getStringExtra("name");
            ic = getIntent().getStringExtra("ic");
            email = getIntent().getStringExtra("email");
            university = getIntent().getStringExtra("university");
            gender = getIntent().getStringExtra("gender");
            setAllFields();

        }

        listener();

    }

    private void listener() {

        if (isFrom.equalsIgnoreCase(Constants.IS_FROM_REGISTER)) {

            nextLinear.setVisibility(View.VISIBLE);
            saveLinear.setVisibility(View.GONE);

        } else if (isFrom.equalsIgnoreCase(Constants.IS_FROM_ACCOUNT_EDIT)) {

            nextLinear.setVisibility(View.GONE);
            saveLinear.setVisibility(View.VISIBLE);
        }

        nextLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields(v);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        relativePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showUploadAlert();

            }
        });

        saveLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateFields(v);

            }
        });

    }

    private void setAllFields() {

        if (MyUtils.checkStringValue(imgUrl)) {
            loadImageSource(imgUrl);
        }
        if (MyUtils.checkStringValue(userName)) {
            edtUserName.setText(userName);
        }
        if (MyUtils.checkStringValue(ic)) {
            edtIc.setText(ic);
        }
        if (MyUtils.checkStringValue(email)) {
            edtEmail.setText(email);
        }
        if (MyUtils.checkStringValue(university)) {
            edtUniversity.setText(university);
        }

        if (MyUtils.checkStringValue(gender)) {
            if (gender.equalsIgnoreCase("0")) {
                radioMale.setChecked(true);
                radioFemale.setChecked(false);
            } else if (gender.equalsIgnoreCase("1")) {
                radioFemale.setChecked(true);
                radioMale.setChecked(false);
            }
        }

    }


    private void validateFields(View v) {

        String userName = edtUserName.getText().toString().trim();
        String ic = edtIc.getText().toString().trim();
        String eMail = edtEmail.getText().toString().trim();
        String university = edtUniversity.getText().toString().trim();

        if (!MyUtils.checkStringValue(userName)) {
            MyUtils.showSnackBar(v, context, "User Name required");
        } else if ((!MyUtils.checkStringValue(ic))) {
            MyUtils.showSnackBar(v, context, "IC required");
        } else if ((!MyUtils.checkStringValue(eMail))) {
            MyUtils.showSnackBar(v, context, "Email required");
        } else if ((!MyUtils.checkStringValue(university))) {
            MyUtils.showSnackBar(v, context, "University required");
        } else if (!MyUtils.checkStringValue(gender)) {
            MyUtils.showSnackBar(v, context, "Gender required");
        }
       /*
        else if (!MyUtils.checkStringValue(imageFilePath)) {
            MyUtils.showSnackBar(v, context, "Profile picture required");
        }
        */
        else {
            if (MyUtils.isValidEmail(eMail)) {
                if (isFrom.equalsIgnoreCase(Constants.IS_FROM_REGISTER)) {
                    resigterApiCall(userName, ic, eMail, university);
                } else if (isFrom.equalsIgnoreCase(Constants.IS_FROM_ACCOUNT_EDIT)) {
                    editPersonalInfo(userName, ic, eMail, university);
                }

            } else {
                MyUtils.showSnackBar(v, context, "Enter valid email address");
            }
        }
    }

    private void showUploadAlert() {
        UploadAlert uploadAlert = new UploadAlert(this, new UploadAlertListener() {
            @Override
            public void onChooseFileClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_FILES_PERMISSION, PersonalDataActivity.this)) {
                    intentToImageSelection();
                }
            }

            @Override
            public void onCaptureCameraClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_CAMERA_PERMISSION, PersonalDataActivity.this)) {
                    intentToCameraApp();
                }
            }
        });
        uploadAlert.showAlertDialog();
    }


    private void intentToImageSelection() {
        imageFilePath = null;
        MyUtils.intentToImageSelection(this, Constants.PICK_IMAGE_FROM_FILES);
    }

    private void intentToCameraApp() {
        imageFilePath = null;
        imageFilePath = MyUtils.intentToCameraApp(this, Constants.WRITE_STORAGE_CAMERA_PERMISSION);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (requestCode == Constants.PICK_IMAGE_FROM_FILES) {

                        imageFilePath = RealPathUtil.getPath(context, data.getData());
                        Log.e("selectedFile", "path:" + imageFilePath);

                        loadImageSource(imageFilePath);

                    } else if (requestCode == Constants.WRITE_STORAGE_CAMERA_PERMISSION) {

                        if (MyUtils.checkStringValue(imageFilePath)) {
                            Log.e("capturedImagePath", "path:" + imageFilePath);
                            loadImageSource(imageFilePath);
                        }
                    }
                    break;
                case Constants.REGISTRATION_DECLINED:
                    setResult(Constants.REGISTRATION_DECLINED);
                    finish();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }


    private void loadImageSource(String imageFilePath) {
        Glide.with(context).load(imageFilePath).placeholder(R.drawable.user_pic).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(imgPhoto);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_STORAGE_FILES_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToImageSelection();
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(PersonalDataActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            case Constants.WRITE_STORAGE_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    intentToCameraApp();

                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(PersonalDataActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(PersonalDataActivity.this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }

    //======Update User profile Info================================================

    private void resigterApiCall(String userName, String ic, String eMail, String university) {
        try {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Constants.API_KEY));
            map.put("name", RequestBody.create(MediaType.parse("text/plain"), userName));
            map.put("ic", RequestBody.create(MediaType.parse("text/plain"), ic));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), eMail));
            map.put("university", RequestBody.create(MediaType.parse("text/plain"), university));
            map.put("gender", RequestBody.create(MediaType.parse("text/plain"), gender));
            map.put("contact_no", RequestBody.create(MediaType.parse("text/plain"), phoneNumber));
            map.put("password", RequestBody.create(MediaType.parse("text/plain"), password));
            map.put("app_id", RequestBody.create(MediaType.parse("text/plain"), Constants.APP_ID));
            map.put("country_id", RequestBody.create(MediaType.parse("text/plain"), countryId));
            map.put("user_role_id", RequestBody.create(MediaType.parse("text/plain"), Constants.USER_ROLE_ID));
            map.put("fcm_token", RequestBody.create(MediaType.parse("text/plain"), fcmToken));
            map.put("device_id", RequestBody.create(MediaType.parse("text/plain"), deviceId));

            MultipartBody.Part body = null;
            if (MyUtils.checkStringValue(imageFilePath)) {
                File file = new File(imageFilePath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
                body = MultipartBody.Part.createFormData("profile_picture", file.getName(), requestFile);
            } else {
                body = MultipartBody.Part.createFormData("profile_picture", "");
            }

            Log.e("image_path", "file:" + imageFilePath);

            progressDialog = MyUtils.showProgressLoader(context, "Uploading...");

            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<RegisterUserResponse> call = apiService.registerUser(map, body);
            call.enqueue(new Callback<RegisterUserResponse>() {
                @Override
                public void onResponse(Call<RegisterUserResponse> call, Response<RegisterUserResponse> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        Log.e("response", "code" + response.code());
                        RegisterUserResponse userResponse = response.body();
                        if (userResponse != null) {
                            String status = userResponse.getStatus();
                            String error = userResponse.getMessage();
                            Log.e("error", ": " + error);
                            Log.e("status", ": " + status);
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                String userId = userResponse.getData().get(0).getUserId();
                                PreferenceHandler.storePreference(context, Constants.USER_ID, userId);
                                PreferenceHandler.storePreference(context, Constants.USER_LOGIN, Constants.USER_LOGIN);

                                intentToHome();
                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Register", MyUtils.checkStringValue(error) ? error : "Unable to Register User");
                                Log.e("error_message11", ": failed case");
                            }

                        } else {
                            showAlertToUser(Constants.DIALOG_FAILURE, "Register", "Unable to Register User");
                            Log.e("error_message", ": null response");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("error_message11", ": " + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<RegisterUserResponse> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Register Data", "Unable to Register Data");
                    Log.e("error_message", ": " + t.getMessage());
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", "code" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Register Data", "Unable to Register Data");
        }
    }

    private void editPersonalInfo(String userName, String ic, String eMail, String university) {
        try {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Constants.API_KEY));
            map.put("user_id", RequestBody.create(MediaType.parse("text/plain"), userId));
            map.put("name", RequestBody.create(MediaType.parse("text/plain"), userName));
            map.put("ic", RequestBody.create(MediaType.parse("text/plain"), ic));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), eMail));
            map.put("university", RequestBody.create(MediaType.parse("text/plain"), university));
            map.put("gender", RequestBody.create(MediaType.parse("text/plain"), gender));
            Log.e("gender", "" + gender);

            MultipartBody.Part body = null;
            if (MyUtils.checkStringValue(imageFilePath)) {
                File file = new File(imageFilePath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
                body = MultipartBody.Part.createFormData("profile_picture", file.getName(), requestFile);
            } else {
                body = MultipartBody.Part.createFormData("profile_picture", "");
            }

            Log.e("image_path", "file:" + imageFilePath);

            progressDialog = MyUtils.showProgressLoader(context, "Updating...");

            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.updateUserDetails(map, body);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        Log.e("response", "code" + response.code());
                        BaseModel userResponse = response.body();
                        if (userResponse != null) {
                            String status = userResponse.getStatus();
                            String error = userResponse.getMessage();
                            Log.e("error", ": " + error);
                            Log.e("status", ": " + status);
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                MyUtils.showLongToast(context, "Successfully Updated..");
                                finish();
                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update", MyUtils.checkStringValue(error) ? error : "Unable to Update data");
                                Log.e("error_message11", ": failed case");
                            }

                        } else {
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update", "Unable to Update data");
                            Log.e("error_message", ": null response");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("error_message11", ": " + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update Data", "Unable to Update data");
                    Log.e("error_message", ": " + t.getMessage());
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", "code" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update Data", "Unable to Update data");
        }
    }

    private void intentToHome() {
        Intent intent = new Intent(PersonalDataActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, PersonalDataActivity.this);
        finish();
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(PersonalDataActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
            }

            @Override
            public void onDismissedClicked() {
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        RadioButton rb = findViewById(checkedId);
        int genId = rb.getId();
        if (genId == R.id.radioMale) {
            gender = "0";//male
        } else if (genId == R.id.radioFemale) {
            gender = "1";//female
        }

    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, PersonalDataActivity.this);
    }

    private void getFcmToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w("notifications", "getInstanceId failed", task.getException());
                }
                fcmToken = task.getResult().getToken();
                Log.e("fcm_token", "token: " + fcmToken);
            }
        });

        deviceId = MyUtils.getDeviceId(PersonalDataActivity.this);
        Log.e("deviceId", "deviceId: " + deviceId);
    }
}
