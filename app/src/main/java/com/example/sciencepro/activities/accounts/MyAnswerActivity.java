package com.example.sciencepro.activities.accounts;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sciencepro.R;
import com.example.sciencepro.adapters.AnswerItemAdapter;
import com.example.sciencepro.utils.MyUtils;

import java.util.ArrayList;

public class MyAnswerActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<String> answerList = new ArrayList<>();
    private AnswerItemAdapter answerItemAdapter = null;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_answer);


        recyclerView = findViewById(R.id.am_recyclerView);
        imgBack = findViewById(R.id.imgBack);

        linearLayoutManager = new LinearLayoutManager(MyAnswerActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        listeners();

    }

    private void listeners() {

        answerItemAdapter = new AnswerItemAdapter(MyAnswerActivity.this, answerList);
        recyclerView.setAdapter(answerItemAdapter);

    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, MyAnswerActivity.this);
    }
}
