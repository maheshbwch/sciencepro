package com.example.sciencepro.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sciencepro.R;
import com.example.sciencepro.adapters.ImageOnlyQuestionsAdapter;
import com.example.sciencepro.adapters.ImageReadAnswerAdapter;
import com.example.sciencepro.adapters.QuestionReadImageAdapter;
import com.example.sciencepro.fragments.RateOurServiceDialog;
import com.example.sciencepro.interfaces.FileDownloadListener;
import com.example.sciencepro.interfaces.OnItemClicked;
import com.example.sciencepro.models.ReadAnswerModel;
import com.example.sciencepro.models.ReadQuestionModel;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.CreatePdf;
import com.example.sciencepro.utils.MyUtils;
import com.example.sciencepro.utils.PermissionAlert;
import com.example.sciencepro.utils.PermissionRequest;
import com.example.sciencepro.utils.PreferenceHandler;
import com.example.sciencepro.webservice.ApiInterface;
import com.example.sciencepro.webservice.HttpRequest;

import java.io.File;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnswersDetailActivity extends AppCompatActivity {

    private Context context;
    private TextView txtRateOurService, txtTitle, txtDesc, txtQuesDesc;
    private RelativeLayout relativePdf;
    private ImageView imgBack, imgDownload;
    private ProgressBar progressBar;
    private RecyclerView recyclerView, recyclerViewQuestion;
    private String userId = "", questionId = "", status = "";
    private Bitmap bitmap;
    private LinearLayout linearAnswer;

    ArrayList<ReadAnswerModel.Datum> arrayList = new ArrayList<>();
    ArrayList<ReadAnswerModel.AttachmentFile> fileArrayList = new ArrayList<>();
    ArrayList<ReadAnswerModel.QuestionsAttachmentFile> questionsAttachmentFiles = new ArrayList<>();

    ArrayList<ReadQuestionModel.Datum> newArraylist = new ArrayList<>();
    ArrayList<ReadQuestionModel.AttachmentFile> attachmentFiles = new ArrayList<>();


    private int questionPos, ansPos, questionNewPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answers_detail);
        context = AnswersDetailActivity.this;

        txtRateOurService = findViewById(R.id.aad_txtRateServices);
        imgBack = findViewById(R.id.imgBack);
        txtTitle = findViewById(R.id.aad_txtTitle);
        txtQuesDesc = findViewById(R.id.aad_txtQuesDesc);
        txtDesc = findViewById(R.id.txtAnswers);
        imgDownload = findViewById(R.id.aad_downLoad);
        progressBar = findViewById(R.id.fp_progressBar);
        recyclerView = findViewById(R.id.aad_recyclerView);
        recyclerViewQuestion = findViewById(R.id.aad_recyclerViewQues);
        relativePdf = findViewById(R.id.relativePdf);
        linearAnswer = findViewById(R.id.linearAnswer);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        questionId = getIntent().getStringExtra(Constants.QUESTIONS_ID);
        status = getIntent().getStringExtra(Constants.STATUS_QA);

        txtRateOurService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RateOurServiceDialog ratingDialog = new RateOurServiceDialog(AnswersDetailActivity.this);
                ratingDialog.show(AnswersDetailActivity.this.getSupportFragmentManager(), "dialog");
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_FILES_PERMISSION, AnswersDetailActivity.this)) {
                    intentToPdfConverter();
                }
            }
        });


        if (status.equalsIgnoreCase("answered")) {
            getAnswerDetail();
        } else {
            getQuestionDetail();
        }

    }

    private void intentToPdfConverter() {

        Log.d("size", " " + relativePdf.getWidth() + "  " + relativePdf.getWidth());
        bitmap = loadBitmapFromView(relativePdf, relativePdf.getWidth(), relativePdf.getHeight());
        CreatePdf createPdf = new CreatePdf(AnswersDetailActivity.this, bitmap);
        createPdf.performPDFConversion();
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }


    private void getAnswerDetail() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<ReadAnswerModel> call = apiService.readAnswer(Constants.API_KEY, Constants.PLATFORM, questionId);
            call.enqueue(new Callback<ReadAnswerModel>() {
                @Override
                public void onResponse(Call<ReadAnswerModel> call, Response<ReadAnswerModel> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        ReadAnswerModel readAnswerModel = response.body();

                        if (readAnswerModel != null) {
                            String status = readAnswerModel.getStatus();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                updateDetails(readAnswerModel);
                            }
                        } else {
                            Log.e("error", "response un successful");
                        }

                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<ReadAnswerModel> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void updateDetails(ReadAnswerModel readAnswerModel) {

        arrayList = new ArrayList<>();
        fileArrayList = new ArrayList<>();
        questionsAttachmentFiles = new ArrayList<>();
        arrayList.clear();
        fileArrayList.clear();
        questionsAttachmentFiles.clear();
        arrayList.addAll(readAnswerModel.getData());
        fileArrayList.addAll(readAnswerModel.getData().get(0).getAttachmentFile());
        questionsAttachmentFiles.addAll(readAnswerModel.getData().get(0).getQuestionAttachmentFile());

        String title = arrayList.get(0).getTopicDetails();
        String quesDesc = arrayList.get(0).getQuestionDetails();
        String answer = arrayList.get(0).getAnswerDetails();

        if (MyUtils.checkStringValue(title)) {
            txtTitle.setText(title);
        }

        if (MyUtils.checkStringValue(quesDesc)) {
            txtQuesDesc.setText(quesDesc);
        }
        if (MyUtils.checkStringValue(answer)) {
            txtDesc.setText(answer);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);

        recyclerViewQuestion.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewQuestion.setHasFixedSize(true);

        ImageReadAnswerAdapter certificateListAdapter = new ImageReadAnswerAdapter(context, fileArrayList);
        certificateListAdapter.OnItemClicked(new OnItemClicked() {
            @Override
            public void onItemClicked(int position) {
                ansPos = position;

                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_ATTACHMENT_FILE_PERMISSION, AnswersDetailActivity.this)) {
                    intentToDownloadFileFromURL("answer");
                }

            }
        });
        recyclerView.setAdapter(certificateListAdapter);

        QuestionReadImageAdapter questionReadImageAdapter = new QuestionReadImageAdapter(context, questionsAttachmentFiles);
        questionReadImageAdapter.OnItemClicked(new OnItemClicked() {
            @Override
            public void onItemClicked(int position) {
                questionPos = position;

                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_QUESTION_ATTACH_FILE_PERMISSION, AnswersDetailActivity.this)) {
                    intentToDownloadFileFromURL("question");
                }

            }
        });
        recyclerViewQuestion.setAdapter(questionReadImageAdapter);

        linearAnswer.setVisibility(View.VISIBLE);

    }

    private void getQuestionDetail() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<ReadQuestionModel> call = apiService.readQuestions(Constants.API_KEY, Constants.PLATFORM, questionId);

            call.enqueue(new Callback<ReadQuestionModel>() {
                @Override
                public void onResponse(Call<ReadQuestionModel> call, Response<ReadQuestionModel> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        ReadQuestionModel readQuestionModel = response.body();

                        if (readQuestionModel != null) {
                            String status = readQuestionModel.getStatus();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                updateQuestionDetails(readQuestionModel);
                            }
                        } else {
                            Log.e("error", "response un successful");
                        }

                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<ReadQuestionModel> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void updateQuestionDetails(ReadQuestionModel readAnswerModel) {

        newArraylist = new ArrayList<>();
        attachmentFiles = new ArrayList<>();
        newArraylist.clear();
        attachmentFiles.clear();
        newArraylist.addAll(readAnswerModel.getData());
        attachmentFiles.addAll(readAnswerModel.getData().get(0).getAttachmentFile());

        String status = newArraylist.get(0).getStatus();
        String title = newArraylist.get(0).getTopicDetails();
        String quesDesc = newArraylist.get(0).getQuestionDetails();

        if (status.equalsIgnoreCase("Answered")) {
            linearAnswer.setVisibility(View.VISIBLE);
        } else {
            linearAnswer.setVisibility(View.GONE);
        }

        if (MyUtils.checkStringValue(title)) {
            txtTitle.setText(title);
        }

        if (MyUtils.checkStringValue(quesDesc)) {
            txtQuesDesc.setText(quesDesc);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);

        recyclerViewQuestion.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewQuestion.setHasFixedSize(true);

        ImageOnlyQuestionsAdapter questionReadImageAdapter = new ImageOnlyQuestionsAdapter(context, attachmentFiles);
        questionReadImageAdapter.OnItemClicked(new OnItemClicked() {
            @Override
            public void onItemClicked(int position) {
                questionNewPos = position;

                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_NEW_QUES_ATTACH_FILE, AnswersDetailActivity.this)) {
                    intentToDownloadFileFromURL("question_new");
                }

            }
        });
        recyclerViewQuestion.setAdapter(questionReadImageAdapter);
        linearAnswer.setVisibility(View.GONE);

    }

    private void intentToDownloadFileFromURL(String isWhichClick) {

        if (isWhichClick.equalsIgnoreCase("answer")) {
            String imagePath = arrayList.get(0).getAttachmentFile().get(ansPos).getFile();
            File file = new File(imagePath);
            String assetImageName = file.getName();
            String downloadfile = fileArrayList.get(ansPos).getDownloadFile();
            clickFileDownload(assetImageName, downloadfile);
        } else if (isWhichClick.equalsIgnoreCase("question")) {
            String imagePath = arrayList.get(0).getQuestionAttachmentFile().get(questionPos).getFile();
            File file = new File(imagePath);
            String assetImageName = file.getName();
            String downloadfile = questionsAttachmentFiles.get(questionPos).getDownloadFile();
            clickFileDownload(assetImageName, downloadfile);
        } else if (isWhichClick.equalsIgnoreCase("question_new")) {
            String imagePath = newArraylist.get(0).getAttachmentFile().get(questionNewPos).getFile();
            File file = new File(imagePath);
            String assetImageName = file.getName();
            String downloadfile = attachmentFiles.get(questionNewPos).getFile();
            clickFileDownload(assetImageName, downloadfile);
        }

    }

    private void clickFileDownload(String assetImageName, String assetImagePath) {
        if (MyUtils.isConnected(context)) {
            downloadAttachment(assetImageName, assetImagePath);
        } else {
            MyUtils.showLongToast(context, String.valueOf(R.string.internet_failed));
        }
    }

    private void downloadAttachment(String assetImageName, String assetImagePath) {
        if (MyUtils.checkStringValue(assetImagePath) && MyUtils.checkStringValue(assetImageName)) {
            Log.e("url", "" + assetImagePath);
            Log.e("url", "filename: " + assetImageName);
            MyUtils.downloadFileFromUrl(assetImagePath, assetImageName, "File", context, new FileDownloadListener() {
                @Override
                public void onDownloadSuccess(String filePath) {
                    MyUtils.showLongToast(context, "Saved!");
                }

                @Override
                public void onDownloadFailed(String errorMessage) {
                    MyUtils.showLongToast(context, "File not Saved!");
                }
            });
        } else {
            MyUtils.showLongToast(context, Constants.ERROR_DOWNLOAD_FILE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_STORAGE_FILES_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToPdfConverter();
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AnswersDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;

            case Constants.WRITE_ATTACHMENT_FILE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToDownloadFileFromURL("answer");
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AnswersDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;

            case Constants.WRITE_QUESTION_ATTACH_FILE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToDownloadFileFromURL("question");
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AnswersDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;

            case Constants.WRITE_NEW_QUES_ATTACH_FILE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToDownloadFileFromURL("question_new");
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AnswersDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AnswersDetailActivity.this);
    }
}
