package com.example.sciencepro.activities.accounts;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sciencepro.R;
import com.example.sciencepro.utils.MyUtils;

public class SettingsActivity extends AppCompatActivity {

    private LinearLayout linearSettings;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        linearSettings = findViewById(R.id.settingsAccount);
        imgBack = findViewById(R.id.imgBack);

        linearSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SettingsActivity.this, ChangePassword.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, SettingsActivity.this);

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, SettingsActivity.this);
    }
}
