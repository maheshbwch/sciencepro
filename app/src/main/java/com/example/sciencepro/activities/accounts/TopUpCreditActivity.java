package com.example.sciencepro.activities.accounts;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.sciencepro.R;
import com.example.sciencepro.adapters.TopUpItemAdapter;
import com.example.sciencepro.utils.MyUtils;

import java.util.ArrayList;

public class TopUpCreditActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<String> topUpArraList = new ArrayList<>();
    private TopUpItemAdapter topUpCreditActivity = null;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up_credit);


        recyclerView = findViewById(R.id.atu_recyclerView);
        imgBack = findViewById(R.id.imgBack);

        linearLayoutManager = new LinearLayoutManager(TopUpCreditActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);



        listeners();
    }

    private void listeners() {

        topUpCreditActivity = new TopUpItemAdapter(TopUpCreditActivity.this, topUpArraList);
        recyclerView.setAdapter(topUpCreditActivity);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, TopUpCreditActivity.this);
    }
}
