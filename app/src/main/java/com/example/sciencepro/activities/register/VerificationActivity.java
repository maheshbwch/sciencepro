package com.example.sciencepro.activities.register;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.example.sciencepro.R;
import com.example.sciencepro.interfaces.CountDownInterface;
import com.example.sciencepro.interfaces.DialogInterface;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.CustomDialog;
import com.example.sciencepro.utils.MyCountDownTimer;
import com.example.sciencepro.utils.MyUtils;
import com.example.sciencepro.utils.custom_view.otp_view.OnOtpCompletionListener;
import com.example.sciencepro.utils.custom_view.otp_view.OtpView;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class VerificationActivity extends AppCompatActivity {

    private LinearLayout nextLinear;

    private Context context;
    private ImageView imgBack;
    private OtpView otpView;
    private TextView resendCodeTxt, countDownTxt;
    private ProgressDialog progressDialog = null;

    private FirebaseAuth mAuth;
    private MyCountDownTimer myCountDownTimer = null;

    private String phoneNumberWithCC = "";
    private String otpVerificationId;

    private boolean isOtpResend = false;

    private String phoneNumber = "", countryId = "", password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        context = VerificationActivity.this;

        init();

    }

    private void init() {

        nextLinear = findViewById(R.id.nextRegisterVerify);
        imgBack = findViewById(R.id.imgBack);
        countDownTxt = findViewById(R.id.aov_countDownTxt);
        resendCodeTxt = findViewById(R.id.aov_resendCodeTxt);
        otpView = findViewById(R.id.aov_otp_view);

        otpVerificationId = getIntent().getStringExtra(Constants.VERIFICATION_ID);
        phoneNumber = getIntent().getStringExtra(Constants.PHONE_NUMBER);
        password = getIntent().getStringExtra(Constants.PASSWORD);
        countryId = getIntent().getStringExtra(Constants.COUNTRY_CODE_ID);
        phoneNumberWithCC = getIntent().getStringExtra(Constants.PHONE_WITH_CC);

        if (Constants.SKIP_OTP) {
            nextLinear.setFocusable(true);
        } else {
            nextLinear.setFocusable(false);
        }

        listener();
    }

    private void listener() {

        nextLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (nextLinear.isFocusable()) {
                    String OTP = otpView.getText().toString();

                    Log.e("OTP", ":" + OTP);

                    if (Constants.SKIP_OTP) {
                        intentToProfilePage();
                    } else {
                        if (OTP.length() == 6) {
                            verifyOTPCode(OTP);
                        } else {
                            MyUtils.showLongToast(context, "Enter the OTP Number");
                        }
                    }
                }

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        mAuth = FirebaseAuth.getInstance();

        startCountDown();


        otpView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkBottomLinearStatus(s.length() == 6);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                MyUtils.hideKeyboard(VerificationActivity.this);
            }
        });

    }

    private void verifyOTPCode(String code) {
        progressDialog = MyUtils.showProgressLoader(context, "Verifying Code...");
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(otpVerificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(VerificationActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                MyUtils.dismissProgressLoader(progressDialog);
                if (task.isSuccessful()) {
                    intentToProfilePage();
                    Log.e("verification", "code:verified");
                } else {
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        Log.e("verification", "failed:" + task.getException().getMessage());
                        showAlertToUser(Constants.DIALOG_FAILURE, "Invalid OTP", "The OTP that you have entered is invalid", false);
                    }
                    clearOTPEditTxt();
                }
            }
        });
    }

    private void intentToProfilePage() {

        Intent intent = new Intent(VerificationActivity.this, PersonalDataActivity.class);
        intent.putExtra(Constants.IS_FROM,Constants.IS_FROM_REGISTER);
        intent.putExtra(Constants.PHONE_NUMBER, phoneNumber);
        intent.putExtra(Constants.PASSWORD,password);
        intent.putExtra(Constants.COUNTRY_CODE_ID,countryId);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, VerificationActivity.this);
        finish();

    }

    private void checkBottomLinearStatus(boolean state) {
        if (state && !nextLinear.isFocusable()) {
            //Log.e("state", ":" + bottomLinear.isFocusableInTouchMode());
            nextLinear.setFocusable(true);
        } else if (!state && nextLinear.isFocusable()) {
            //Log.e("state", ":" + bottomLinear.isFocusableInTouchMode());
            nextLinear.setFocusable(false);
        }
    }

    private void startCountDown() {
        myCountDownTimer = new MyCountDownTimer(Constants.OTP_TIME_OUT * 1000, 1000, new CountDownInterface() {
            @Override
            public void onTicking(long millisUntilFinished) {
                String text = String.format(Locale.getDefault(), "%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60, TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60);
                countDownTxt.setText("" + text);
            }

            @Override
            public void onFinished() {
                countDownTxt.setText("00:00");
                if (!isOtpResend) {
                    resendCodeTxt.setAlpha(1);
                } else {
                    resendCodeTxt.setAlpha((float) 0.3);
                }
                resendCodeTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isOtpResend) {
                            sendVerificationCode(phoneNumberWithCC);
                        }
                    }
                });


            }
        });
        myCountDownTimer.start();
    }

    private void sendVerificationCode(String mobile) {
        progressDialog = MyUtils.showProgressLoader(context, "Sending Code...");
        PhoneAuthProvider provider = PhoneAuthProvider.getInstance();
        provider.verifyPhoneNumber(mobile, Constants.OTP_TIME_OUT, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD, mCallbacks);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.e("verification", "code:" + code);
            MyUtils.dismissProgressLoader(progressDialog);

            Toast.makeText(context, "code:" + code, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            if (e instanceof FirebaseTooManyRequestsException) {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The entered phone number : " + phoneNumberWithCC + " is attempted too many OTP verification. Please try after some time", false);
            } else {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The entered phone number : " + phoneNumberWithCC + " is verification failed. Please try again", false);
            }

            Log.e("verification", "exp:" + e.getMessage());
            MyUtils.dismissProgressLoader(progressDialog);
            Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            otpVerificationId = verificationId;
            Log.e("verification", "id:" + verificationId);
            MyUtils.dismissProgressLoader(progressDialog);

            resendCodeTxt.setAlpha((float) 0.3);
            isOtpResend = true;

            startCountDown();
        }
    };

    private void showAlertToUser(String statusCode, String title, String statusMessage, final boolean isClose) {
        CustomDialog customDialog = new CustomDialog(VerificationActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                if (isClose) {
                    closePage();
                }
            }

            @Override
            public void onDismissedClicked() {
                if (isClose) {
                    closePage();
                }
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }

    private void clearOTPEditTxt() {
        otpView.setText("");
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, VerificationActivity.this);
    }
}
