package com.example.sciencepro.interfaces;


import com.example.sciencepro.models.WorkTypes;

public interface WorkTypeListener {
    void onItemClicked(WorkTypes workTypes);
}
