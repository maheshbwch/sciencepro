package com.example.sciencepro.interfaces;

public interface OnItemClicked {
    void onItemClicked(int position);
}
