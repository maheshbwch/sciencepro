package com.example.sciencepro.interfaces;

public interface CountDownInterface {
    void onTicking(long millisUntilFinished);
    void onFinished();

}
