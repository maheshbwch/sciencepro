package com.example.sciencepro.interfaces;

public interface UpdateDialogListener {
    void onUpdateButtonClicked();

    void onNotNowButtonClicked();

    void onDismissButtonClicked();

}
