package com.example.sciencepro.interfaces;

public interface ConnectivityInterface {
    void onConnected();
    void onNotConnected();
}
