package com.example.sciencepro.interfaces;

public interface CustomAlertInterface {

    void onPositiveButtonClicked();

    void onNegativeButtonClicked();

}
