package com.example.sciencepro.interfaces;

public interface UploadAlertListener {
    void onChooseFileClicked();
    void onCaptureCameraClicked();
}
