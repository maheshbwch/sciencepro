package com.example.sciencepro.interfaces;

public interface DialogInterface {
    void onButtonClicked();
    void onDismissedClicked();
}
