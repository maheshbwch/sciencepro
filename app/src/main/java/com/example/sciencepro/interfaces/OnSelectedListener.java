package com.example.sciencepro.interfaces;


import com.example.sciencepro.models.CountryCodes;

public interface OnSelectedListener {

    void onSelected(CountryCodes.Datum countryCodes);
    void onNothingSelected();

}