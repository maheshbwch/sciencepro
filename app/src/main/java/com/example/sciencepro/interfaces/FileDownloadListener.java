package com.example.sciencepro.interfaces;

public interface FileDownloadListener {

    void onDownloadSuccess(String filePath);
    void onDownloadFailed(String errorMessage);

}
