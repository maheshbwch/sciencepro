package com.example.sciencepro.interfaces;

public interface AttachmentListener {
    void onImageRemoved(int position);
    void onImageChanged(int position);
}
