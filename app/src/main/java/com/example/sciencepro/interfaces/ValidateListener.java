package com.example.sciencepro.interfaces;

public interface ValidateListener {
    void onValidated();
}
