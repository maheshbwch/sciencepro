package com.example.sciencepro.webservice;

public class ApiConstants {

    public static final String BASE_URL = "http://thesispro.bestweb.my/";
    public static final String API_BASE_URL = BASE_URL + "api/";

    public static final String COUNTRIES_CODES_LIST = API_BASE_URL + "location/get_countries";
    public static final String USER_VALIDATION = API_BASE_URL + "user/user_contact_no_exist_or_not__checking";
    public static final String REGISTER_USER = API_BASE_URL + "user/register_user";
    public static final String LOGIN_USER = API_BASE_URL + "user/login";
    public static final String GET_USER_DETAILS = API_BASE_URL + "user/read_personal_info";
    public static final String GET_CHAPTER_LIST = API_BASE_URL + "Chapter/chapter_list";
    public static final String GET_TOPIC_LIST = API_BASE_URL + "Chapter/topic_list";
    public static final String POST_QUESTIONS = API_BASE_URL + "Question_student_app/post_question";
    public static final String GET_RECENT_QUESTIONS = API_BASE_URL + "Question_student_app/question_list";
    public static final String UPDATE_PERSONAL_DETAILS = API_BASE_URL + "user/edit_user_details";
    public static final String READ_ANSWER = API_BASE_URL + "Lecture_app_question/read_answer";
    public static final String GET_HISTORY_LIST = API_BASE_URL + "Question_student_app/history_list";
    public static final String SUBMIT_RATING = API_BASE_URL + "Question_student_app/student_rating";
    public static final String CHANGE_PASSWORD = API_BASE_URL + "User/change_password";
    public static final String READ_QUESTIONS = API_BASE_URL + "Lecture_app_question/read_question";
    public static final String NOTIFICATON_LIST = API_BASE_URL + "Question_student_app/notification_list";

}
