package com.example.sciencepro.webservice;

import com.example.sciencepro.models.BaseModel;
import com.example.sciencepro.models.ChapterResponse;
import com.example.sciencepro.models.CountryCodes;
import com.example.sciencepro.models.HistoryListModel;
import com.example.sciencepro.models.LoginResponse;
import com.example.sciencepro.models.NotificationListModel;
import com.example.sciencepro.models.ReadAnswerModel;
import com.example.sciencepro.models.ReadQuestionModel;
import com.example.sciencepro.models.RecentQuestionsModel;
import com.example.sciencepro.models.RegisterUserResponse;
import com.example.sciencepro.models.TopicResponse;
import com.example.sciencepro.models.UserDetailsResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ApiInterface {

    @FormUrlEncoded
    @POST(ApiConstants.COUNTRIES_CODES_LIST)
    Call<CountryCodes> getCountriesCodesList(@Field("api_key") String apiKey);

    @FormUrlEncoded
    @POST(ApiConstants.USER_VALIDATION)
    Call<BaseModel> userValidation(@Field("api_key") String apiKey, @Field("contact_no") String contactNo, @Field("app_id") String appId, @Field("country_id") String countryId, @Field("user_role_id") String userRoleId);

    @Multipart
    @POST(ApiConstants.REGISTER_USER)
    Call<RegisterUserResponse> registerUser(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST(ApiConstants.LOGIN_USER)
    Call<LoginResponse> loginUser(@Field("api_key") String apiKey, @Field("contact_no") String phone, @Field("password") String password,
                                  @Field("fcm_token") String fcmToken, @Field("device_id") String deviceId, @Field("app_id") String appId);

    @FormUrlEncoded
    @POST(ApiConstants.GET_USER_DETAILS)
    Call<UserDetailsResponse> getAccountDetails(@Field("api_key") String apiKey, @Field("user_id") String userId);

    @FormUrlEncoded
    @POST(ApiConstants.GET_CHAPTER_LIST)
    Call<ChapterResponse> getChapterList(@Field("api_key") String apiKey,@Field("app_id") String appId);


    @FormUrlEncoded
    @POST(ApiConstants.GET_TOPIC_LIST)
    Call<TopicResponse> getTopicList(@Field("api_key") String apiKey, @Field("chapter_id") String chapterId);

    @Multipart
    @POST(ApiConstants.POST_QUESTIONS)
    Call<BaseModel> postQuestion(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file1,
                                 @Part MultipartBody.Part file2, @Part MultipartBody.Part file3, @Part MultipartBody.Part file4, @Part MultipartBody.Part file5);

    @FormUrlEncoded
    @POST(ApiConstants.GET_RECENT_QUESTIONS)
    Call<RecentQuestionsModel> getRecentQuestion(@Field("api_key") String apiKey, @Field("student_id") String userId,
                                                 @Field("app_id") String appId, @Field("pagination") String pagination);


    @Multipart
    @POST(ApiConstants.UPDATE_PERSONAL_DETAILS)
    Call<BaseModel> updateUserDetails(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST(ApiConstants.READ_ANSWER)
    Call<ReadAnswerModel> readAnswer(@Field("api_key") String apiKey, @Field("platform_type") String platformId, @Field("question_id") String questionId);

    @FormUrlEncoded
    @POST(ApiConstants.GET_HISTORY_LIST)
    Call<HistoryListModel> getHistoryList(@Field("api_key") String apiKey, @Field("student_id") String studentId, @Field("platform_type") String platformId,
                                          @Field("pagination") String pagination, @Field("filter_status") String filterStatus, @Field("date_order_by") String dateOrderby);

    @FormUrlEncoded
    @POST(ApiConstants.SUBMIT_RATING)
    Call<BaseModel> submitRating(@Field("api_key") String apiKey, @Field("student_id") String userId, @Field("rating") String rating,
                                 @Field("suggestion") String suggestion);

    @FormUrlEncoded
    @POST(ApiConstants.CHANGE_PASSWORD)
    Call<BaseModel> changePassword(@Field("api_key") String apiKey, @Field("platform_type") String platformType, @Field("user_id") String userId,
                                   @Field("old_password") String oldPass, @Field("new_password") String newPass);

    @GET
    Call<ResponseBody> downloadImage(@Url String url);

    @FormUrlEncoded
    @POST(ApiConstants.READ_QUESTIONS)
    Call<ReadQuestionModel> readQuestions(@Field("api_key") String apiKey, @Field("platform_type") String platFormType,
                                          @Field("question_id") String questionId);


    @FormUrlEncoded
    @POST(ApiConstants.NOTIFICATON_LIST)
    Call<NotificationListModel> getNotification(@Field("api_key") String apiKey, @Field("pagination") String pagination, @Field("student_id") String studentId);


}
