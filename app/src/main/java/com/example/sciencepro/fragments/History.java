package com.example.sciencepro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.sciencepro.R;
import com.example.sciencepro.adapters.FilterListAdapter;
import com.example.sciencepro.adapters.HistoryItemAdapter;
import com.example.sciencepro.interfaces.WorkTypeListener;
import com.example.sciencepro.models.HistoryListModel;
import com.example.sciencepro.models.WorkTypes;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.MyUtils;
import com.example.sciencepro.utils.PreferenceHandler;
import com.example.sciencepro.webservice.ApiInterface;
import com.example.sciencepro.webservice.HttpRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class History extends Fragment {


    private View rootView = null;
    private Context context;
    private SwipeRefreshLayout swipeToRefreshLayout;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressBar, bottomProgressBar;


    private boolean isLoading = true;
    private int pageNumber = 0;
    private String userId = "";

    private String dateSort = Constants.DESCENDING;
    private String dateSelected = "";
    private String typeOfService = "";

    private RecyclerView filterRecycler;
    private ArrayList<WorkTypes> filterArrayList = new ArrayList<>();
    private FilterListAdapter filterListAdapter = null;

    private ArrayList<HistoryListModel.Datum> historyListModelArrayList = new ArrayList<>();
    private HistoryItemAdapter historyItemAdapter = null;

    public History() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_history, container, false);
        }

        init(rootView);

        return rootView;
    }

    private void init(View view) {

        context = getActivity();
        swipeToRefreshLayout = view.findViewById(R.id.fa_swipeToRefreshLayout);
        filterRecycler = view.findViewById(R.id.fa_filterRecycler);
        recyclerView = view.findViewById(R.id.fa_recyclerView);

        progressBar = view.findViewById(R.id.fa_progressBar);
        bottomProgressBar = view.findViewById(R.id.fa_bottomProgressBar);

        userId = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.USER_ID);

        listeners();
    }

    private void listeners() {


        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        filterRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        filterRecycler.setHasFixedSize(true);


        swipeToRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefreshLayout.setRefreshing(false);

                getHistoryList(0, true);

            }
        });


        updateFilterListUI();
    }

    private void updateFilterListUI() {
        filterArrayList.clear();
        filterArrayList.add(new WorkTypes(Constants.DESCENDING, Constants.DATE_AND_TIME, true));
        filterArrayList.add(new WorkTypes(Constants.SERVICE_TYPE_CODE1, Constants.SERVICE_TYPE1, false));
        filterArrayList.add(new WorkTypes(Constants.SERVICE_TYPE_CODE2, Constants.SERVICE_TYPE2, false));
        filterArrayList.add(new WorkTypes(Constants.SERVICE_TYPE_CODE3, Constants.SERVICE_TYPE3, false));
        filterArrayList.add(new WorkTypes(Constants.SERVICE_TYPE_CODE4, Constants.SERVICE_TYPE4, false));
        filterArrayList.add(new WorkTypes(Constants.SERVICE_TYPE_CODE5, Constants.SERVICE_TYPE5, false));


        filterListAdapter = new FilterListAdapter(getActivity(), filterArrayList);
        filterListAdapter.setOnItemClicked(new WorkTypeListener() {
            @Override
            public void onItemClicked(WorkTypes workTypes) {
                String id = workTypes.getId();

                if (id.equalsIgnoreCase(Constants.ASCENDING) || id.equalsIgnoreCase(Constants.DESCENDING)) {
                    dateSort = id;
                    typeOfService = "";
                } else {
                    typeOfService = id;
                }

                getHistoryList(0, true);

            }
        });
        filterRecycler.setAdapter(filterListAdapter);

        dateSort = Constants.DESCENDING;

        getHistoryList(0,true);
    }

    //========Handle filter result========================================================================

    public void getFilterValues(String selectedDate, String serviceType) {
        dateSelected = selectedDate;
        typeOfService = serviceType;

        if (filterArrayList.size() > 0) {

            filterArrayList.get(0).setName(MyUtils.checkStringValue(dateSelected) ? dateSelected : Constants.DATE_AND_TIME);
            setSelectFilterArrayList(0);

            if (MyUtils.checkStringValue(serviceType)) {
                switch (serviceType) {
                    case Constants.SERVICE_TYPE_CODE1:
                        setSelectFilterArrayList(1);
                        notifyItemChanged(1);
                        break;
                    case Constants.SERVICE_TYPE_CODE2:
                        setSelectFilterArrayList(2);
                        notifyItemChanged(2);
                        break;
                    case Constants.SERVICE_TYPE_CODE3:
                        setSelectFilterArrayList(3);
                        notifyItemChanged(2);
                        break;
                    default:
                        break;
                }

            } else {
                notifyItemChanged(0);
            }
        }

        getHistoryList(0, true);
    }

    private void setSelectFilterArrayList(int position) {
        for (int i = 0; i < filterArrayList.size(); i++) {
            if (position == i) {
                filterArrayList.get(i).setSelected(true);
            } else {
                filterArrayList.get(i).setSelected(false);
            }
        }
    }


    private void notifyItemChanged(int position) {
        if (filterListAdapter != null && recyclerView != null) {
            filterListAdapter.notifyDataSetChanged();
            recyclerView.scrollToPosition(position);
        }
    }

    private void getHistoryList(int number, final boolean isLoadingForFirstTime) {
        try {
            if (isLoadingForFirstTime) {
                isLoading = true;
                pageNumber = 0;
                historyListModelArrayList.clear();
                progressBar.setVisibility(View.VISIBLE);
            } else {
                bottomProgressBar.setVisibility(View.VISIBLE);
            }
            Log.e("typeOfService", "" + typeOfService);
            Log.e("dateSort", "" + dateSort);
            Log.e("userId", "" + userId);

            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<HistoryListModel> call = apiService.getHistoryList(Constants.API_KEY, userId, Constants.PLATFORM,
                    String.valueOf(pageNumber), typeOfService, dateSort);
            call.enqueue(new Callback<HistoryListModel>() {
                @Override
                public void onResponse(Call<HistoryListModel> call, Response<HistoryListModel> response) {
                    hideProgressBar(isLoadingForFirstTime);
                    if (response.isSuccessful()) {
                        HistoryListModel historyListModel = response.body();

                        if (historyListModel != null) {
                            String status = historyListModel.getStatus();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                adaptToRecycler(historyListModel);
                            }
                        } else {
                            Log.e("error", "response un successful");
                        }

                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<HistoryListModel> call, Throwable t) {
                    hideProgressBar(isLoadingForFirstTime);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            hideProgressBar(isLoadingForFirstTime);
            if (!isLoadingForFirstTime) {
                isLoading = isLoadingForFirstTime;
            }
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void hideProgressBar(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.GONE);
            }
        } else {
            hideBottomProgressBar();
        }
    }

    private void hideBottomProgressBar() {
        if (bottomProgressBar.getVisibility() == View.VISIBLE) {
            bottomProgressBar.setVisibility(View.GONE);
        }
    }

    private void adaptToRecycler(HistoryListModel historyListModel) {


        historyListModelArrayList.addAll(historyListModel.getData());

        if (historyListModelArrayList.size() > 0) {

            if (historyItemAdapter != null) {
                historyItemAdapter.notifyDataSetChanged();
                isLoading = true;
            } else {
                historyItemAdapter = new HistoryItemAdapter(context, historyListModelArrayList);

                recyclerView.setAdapter(historyItemAdapter);
            }

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) //check for scroll down
                    {
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                isLoading = false;
                                pageNumber = pageNumber + 1;

                                getHistoryList(pageNumber, false);

                                Log.e("recyclerView", "bottom_reached");
                                Log.e("page_number", "number:" + pageNumber);
                            }
                        }
                    }
                }
            });
        }

    }


}
