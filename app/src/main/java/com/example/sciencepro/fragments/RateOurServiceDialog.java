package com.example.sciencepro.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.example.sciencepro.R;
import com.example.sciencepro.models.BaseModel;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.MyUtils;
import com.example.sciencepro.utils.PreferenceHandler;
import com.example.sciencepro.webservice.ApiInterface;
import com.example.sciencepro.webservice.HttpRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateOurServiceDialog extends DialogFragment {

    private Context context;
    private ProgressDialog progressDialog = null;
    private String userId = "";


    public RateOurServiceDialog(Context context) {
        this.context = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View rootView = inflater.inflate(R.layout.rate_our_service_dialog, null);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        setUpView(rootView);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setView(rootView);

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        //dialog.setCancelable(false);
        return dialog;
    }


    private void setUpView(View rootView) {


        ImageView closeImage = rootView.findViewById(R.id.dmr_closeImage);
        RatingBar ratingBar = rootView.findViewById(R.id.dmr_ratingBar);
        EditText descriptionEdt = rootView.findViewById(R.id.dmr_descriptionTxt);
        TextView confirmTxt = rootView.findViewById(R.id.dmr_confirmButton);


        confirmTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float ratingOrinal = ratingBar.getRating();
                int rating = Math.round(ratingOrinal);
                Log.e("rating", ":" + rating);

                String description = "" + descriptionEdt.getText().toString().trim();

                if (rating > 0) {
                    saveCustomerRating(String.valueOf(rating), description);
                } else {
                    Toast.makeText(context, "Rating Required", Toast.LENGTH_SHORT).show();
                }
            }
        });

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    private void saveCustomerRating(String rating, String description) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Submitting...");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.submitRating(Constants.API_KEY, userId, rating, description);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        BaseModel saveOrder = response.body();
                        if (saveOrder != null) {
                            String status = saveOrder.getStatus();
                            String error = saveOrder.getMessage();
                            if (MyUtils.checkStringValue(status)) {
                                if (status.equalsIgnoreCase(Constants.SUCCESS)) {
                                    // showAlertToUser(Constants.DIALOG_SUCCESS, "Review Maid", MyUtils.checkStringValue(error) ? error : "Maid Reviewed successfully");
                                    MyUtils.showLongToast(context, "Thanks for submit the rating..");
                                    closePage();

                                } else if (status.equalsIgnoreCase(Constants.FAILURE)) {
                                    MyUtils.showLongToast(context, "Unable to submit the rating Try again ..");
                                }
                            } else {
                                closePage();
                            }
                        } else {
                            Log.e("error", "null response");
                            MyUtils.showLongToast(context, "Unable to submit the rating Try again ..");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        MyUtils.showLongToast(context, "Unable to submit the rating Try again ..");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    MyUtils.showLongToast(context, "Unable to submit the rating Try again ..");
                }
            });
        } catch (Exception e) {
            closePage();
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void closePage() {
        dismiss();
    }


    @Override
    public void onPause() {
        super.onPause();
        closePage();
    }
}
