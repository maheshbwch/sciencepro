package com.example.sciencepro.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.sciencepro.R;
import com.example.sciencepro.interfaces.OnItemClicked;
import com.example.sciencepro.models.ReadAnswerModel;
import com.example.sciencepro.utils.MyUtils;

import java.io.File;
import java.util.ArrayList;

public class QuestionReadImageAdapter extends RecyclerView.Adapter<QuestionReadImageAdapter.ViewHolder> {

    private ArrayList<ReadAnswerModel.QuestionsAttachmentFile> arrayList;
    private Context context;
    private OnItemClicked onItemClicked;


    public QuestionReadImageAdapter(Context context, ArrayList<ReadAnswerModel.QuestionsAttachmentFile> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void OnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_image_answer, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String imagePath = arrayList.get(position).getFile();
        File file = new File(imagePath);

        Log.e("adapter", ":" + imagePath);
        Log.e("getAbsolutePath", ":" + file.getName());

        if (MyUtils.checkStringValue(imagePath)) {
            //holder.itemView.setVisibility(View.VISIBLE);
            //holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            Glide.with(context).load(imagePath).placeholder(R.drawable.icons_pdf).into(holder.certificateImage);
        }

        if (MyUtils.checkStringValue(file.getName())) {
            holder.txtFilename.setText(file.getName());
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout itemLinear;
        public ImageView certificateImage;
        public TextView txtFilename;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemLinear = itemView.findViewById(R.id.ici_itemLinear);
            this.certificateImage = itemView.findViewById(R.id.ici_certificateImage);
            this.txtFilename = itemView.findViewById(R.id.ici_txtFileName);

            txtFilename.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClicked.onItemClicked(getAdapterPosition());
                }
            });

        }

    }
}
