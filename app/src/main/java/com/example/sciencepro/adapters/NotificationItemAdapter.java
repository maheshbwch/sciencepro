package com.example.sciencepro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.sciencepro.R;
import com.example.sciencepro.interfaces.OnItemClicked;
import com.example.sciencepro.models.NotificationListModel;
import com.example.sciencepro.utils.MyUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class NotificationItemAdapter extends RecyclerView.Adapter<NotificationItemAdapter.ViewHolder> {

    private ArrayList<NotificationListModel.Datum> arrayList;
    private Context context;
    private OnItemClicked onItemClicked = null;


    public NotificationItemAdapter(Context context, ArrayList<NotificationListModel.Datum> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_notification_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        String dateTimeAgo = "";
        String titleName = arrayList.get(position).getTopicDetails();
        String createdDate = arrayList.get(position).getCreatedAt();
        String status = arrayList.get(position).getStatus();

        if (MyUtils.checkStringValue(titleName)) {
            if (status.equalsIgnoreCase("Answered")) {
                holder.txtTitle.setText("Your Question for " + titleName + " has been answered.");
                Glide.with(context).load(R.drawable.question_answered).into(holder.imgView);
            } else if (status.equalsIgnoreCase("Declined")) {
                holder.txtTitle.setText("Your Question for " + titleName + " has been declined.");
                Glide.with(context).load(R.drawable.rejected).into(holder.imgView);
            }
        }

        if (!createdDate.isEmpty()) {
            if (createdDate.contains(" ")) {
                String[] splitWords = createdDate.split("\\s+");
                String datee = splitWords[0];
                String timee = splitWords[1];
                dateTimeAgo = datee + "T" + timee;
            }
        }

        if (!dateTimeAgo.isEmpty()) {
            holder.txtHours.setText(getTimeAgo(dateTimeAgo));
        }

    }

    private String getTimeAgo(String dateTimeAgo) {
        /* String str_date = "2019-10-22T13:00:00";*/
        DateFormat formatter;
        Date date = null;
        //formatter = new SimpleDateFormat("dd-MM-yyyy");
        formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        try {
            date = formatter.parse(dateTimeAgo);
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        Timestamp timeStampDate = new Timestamp(date.getTime());
        String timeAgo = MyUtils.TimeAgo.getTimeAgo(timeStampDate.getTime());

        return timeAgo;

    }


    @Override
    public int getItemCount() {
        return arrayList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle, txtHours;
        private ImageView imgView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgView = itemView.findViewById(R.id.inl_imgView);

            this.txtTitle = itemView.findViewById(R.id.inl_txtTitle);
            this.txtHours = itemView.findViewById(R.id.inl_txtTimeAgo);


        }
    }


}
