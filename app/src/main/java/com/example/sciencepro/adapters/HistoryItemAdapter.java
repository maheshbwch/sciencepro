package com.example.sciencepro.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.sciencepro.R;
import com.example.sciencepro.activities.AnswersDetailActivity;
import com.example.sciencepro.models.HistoryListModel;
import com.example.sciencepro.utils.Constants;
import com.example.sciencepro.utils.MyUtils;

import java.util.ArrayList;

public class HistoryItemAdapter extends RecyclerView.Adapter<HistoryItemAdapter.ViewHolder> {

    private ArrayList<HistoryListModel.Datum> arrayList;
    private Context context;


    public HistoryItemAdapter(Context context, ArrayList<HistoryListModel.Datum> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_history_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String isTopUp = arrayList.get(position).getIs_top_up_row();
        String topUpBalance = arrayList.get(position).getTopUpBalance();
        String txtTitle = arrayList.get(position).getTopicDetails();
        String txtDate = arrayList.get(position).getCreatedAt();
        String txtStatus = arrayList.get(position).getStatus();
        String txtTimeTaken = arrayList.get(position).getTimeTaken();
        String txtDesc = arrayList.get(position).getQuestionDetails();
        String txtQuation = arrayList.get(position).getQuotation();

        if (isTopUp.equalsIgnoreCase("0")) {

            if (txtStatus.equalsIgnoreCase("Answered")) {

                holder.linearTop.setVisibility(View.VISIBLE);
                holder.linearMed.setVisibility(View.VISIBLE);
                holder.linearDown.setVisibility(View.VISIBLE);
                holder.linearSeeAnswer.setVisibility(View.VISIBLE);
                holder.linearMain.setBackgroundColor(context.getResources().getColor(R.color.color_history));
                holder.txtTitleTop.setText("Credit Deduction");
                Glide.with(context).load(R.drawable.question_answered).into(holder.imgView);

                if (MyUtils.checkStringValue(txtQuation)) {
                    holder.txtPrice.setText(txtQuation);
                }
            } else {
                holder.linearTop.setVisibility(View.GONE);
                holder.linearMed.setVisibility(View.VISIBLE);
                holder.linearDown.setVisibility(View.VISIBLE);
                holder.linearSeeAnswer.setVisibility(View.GONE);
                holder.linearMain.setBackgroundColor(context.getResources().getColor(R.color.white));

            }


        } else if (isTopUp.equalsIgnoreCase("1")) {
            Glide.with(context).load(R.drawable.credit).into(holder.imgView);
            holder.txtTitleTop.setText("Credit Top Up");
            holder.linearTop.setVisibility(View.VISIBLE);
            holder.linearMed.setVisibility(View.GONE);
            holder.linearDown.setVisibility(View.GONE);
            holder.linearMain.setBackgroundColor(context.getResources().getColor(R.color.white));
            if (MyUtils.checkStringValue(topUpBalance)) {
                holder.txtPrice.setText(topUpBalance);
            }

        }

        if (MyUtils.checkStringValue(txtTitle)) {
            holder.txtTitle.setText(txtTitle);
        }
        if (MyUtils.checkStringValue(txtDate)) {
            holder.txtDate.setText(txtDate);
        }

        if (MyUtils.checkStringValue(txtStatus)) {
            holder.txtStatus.setText(txtStatus);
        }

        if (MyUtils.checkStringValue(txtTimeTaken)) {
            holder.txtTimeTaken.setText(txtTimeTaken);
        }

        if (MyUtils.checkStringValue(txtDesc)) {
            holder.txtDesc.setText(txtDesc);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle, txtDate, txtStatus, txtTimeTaken, txtTitleTop, txtDesc, txtPrice;
        private ImageView imgView;
        private LinearLayout linearTop, linearMed, linearDown, linearMain, linearSeeAnswer;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtTitle = itemView.findViewById(R.id.txtTitle);
            this.txtDate = itemView.findViewById(R.id.txtDate);
            this.txtStatus = itemView.findViewById(R.id.irh_txtAnswerStatus);
            this.txtTimeTaken = itemView.findViewById(R.id.txtTimeTaken);
            this.imgView = itemView.findViewById(R.id.ihl_imgView);
            this.linearTop = itemView.findViewById(R.id.linearTopUp);
            this.linearMed = itemView.findViewById(R.id.linearMed);
            this.linearDown = itemView.findViewById(R.id.linearDown);
            this.txtTitleTop = itemView.findViewById(R.id.txtTitleTop);
            this.txtDesc = itemView.findViewById(R.id.txtDesc);
            this.txtPrice = itemView.findViewById(R.id.txtPrice);
            this.linearMain = itemView.findViewById(R.id.linearMain);
            this.linearSeeAnswer = itemView.findViewById(R.id.linearSeeAnswer);

            linearSeeAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();

                    String questionId = arrayList.get(position).getQuestionId();
                    String status = arrayList.get(position).getStatus();

                    Intent intent = new Intent(context, AnswersDetailActivity.class);
                    intent.putExtra(Constants.QUESTIONS_ID,questionId);
                    intent.putExtra(Constants.STATUS_QA,status);
                    context.startActivity(intent);
                }
            });
        }
    }


}
