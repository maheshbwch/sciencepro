package com.example.sciencepro.utils.custom_spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sciencepro.R;
import com.example.sciencepro.utils.MyUtils;

import java.util.ArrayList;

public class SpinnerAdapter extends RecyclerView.Adapter<SpinnerAdapter.ViewHolder> {

    private ArrayList<?> arrayList;
    private Context context;
    private OnItemSelected onItemSelected;
    private boolean isBanList;

    public SpinnerAdapter(Context context, ArrayList<?> arrayList, OnItemSelected onItemSelected, boolean isBanList) {
        this.context = context;
        this.arrayList = arrayList;
        this.onItemSelected = onItemSelected;
        this.isBanList = isBanList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_custom_spinner, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

            String name = arrayList.get(position).toString();
            holder.nameTxt.setText(MyUtils.checkStringValue(name) ? name.trim() : "-");

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTxt;
        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.nameTxt = itemView.findViewById(R.id.ics_nameTxt);
            this.imageView = itemView.findViewById(R.id.ics_imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemSelected.onItemSelected(getAdapterPosition());
                }
            });

        }
    }
}