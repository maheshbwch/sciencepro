package com.example.sciencepro.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class CreatePdf {

    private Activity activity;
    private Bitmap bitmap;

    public CreatePdf(Activity activity,Bitmap bitmap) {

        this.activity =activity;
        this.bitmap =bitmap;
    }

    public void performPDFConversion(){

        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        //  Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels;
        float width = displaymetrics.widthPixels;

        int convertHighet = (int) hight, convertWidth = (int) width;

//        Resources mResources = getResources();
//        Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.screenshot);

        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        document.finishPage(page);

        //First Check if the external storage is writable
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            Toast.makeText(activity, "No space!", Toast.LENGTH_SHORT).show();
        }else {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/SciencePRO/" + "/answers/");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
            File file = new File(myDir, UUID.randomUUID().toString()+".pdf");
            if (file.exists()) {
                file.delete();
            }
            try {
                document.writeTo(new FileOutputStream(file));

                Toast.makeText(activity, "PDF is created!!!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(activity, "Something wrong: Try again!" + e.toString(), Toast.LENGTH_LONG).show();
            }
        }

        // close the document
        document.close();

    }

}
