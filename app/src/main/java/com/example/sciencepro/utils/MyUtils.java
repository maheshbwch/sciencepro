package com.example.sciencepro.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.google.android.material.snackbar.Snackbar;
import com.example.sciencepro.BuildConfig;
import com.example.sciencepro.R;
import com.example.sciencepro.interfaces.FileDownloadListener;
import com.example.sciencepro.webservice.ApiInterface;
import com.example.sciencepro.webservice.HttpRequest;

import org.joda.time.LocalDate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyUtils {

    public static ProgressDialog progressDialog;

    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static void openOverrideAnimation(boolean isOpen, Activity activity) {
        activity.overridePendingTransition(isOpen ? R.anim.slide_from_right : R.anim.slide_from_left, isOpen ? R.anim.slide_to_left : R.anim.slide_to_right);
    }

    public static Typeface getBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Kastelov-Axiforma-Bold.otf");
    }

    public static Typeface getMedBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Kastelov-Axiforma-Medium.otf");
    }

    public static Typeface getRegularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Kastelov-Axiforma-Regular.otf");
    }

    public static boolean checkStringValue(String value) {
        return value != null && !value.equalsIgnoreCase("null") && !value.isEmpty();
    }


    public static void showLongToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    public static void showSnackBar(View view,Context context,String msg){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                .setActionTextColor(context.getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    public static ProgressDialog showProgressLoader(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    public static void dismissProgressLoader(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void intentToImageSelection(Activity activity, int code) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), code);
    }

    public static String intentToCameraApp(Activity activity, int code) {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile(activity);
            } catch (IOException ex) {
                //Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(pictureIntent, code);

                return photoFile.getAbsolutePath();
            }
        }
        return null;
    }

    public static File createImageFile(Activity activity) throws IOException {
       /* File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        file.createNewFile();
        return  file;*/
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(getImageName(), ".jpg", storageDir);
    }

    public static String getImageName() {
        return ""+Constants.APP_NAME + getCurrentTimeStamp();
    }

    public static String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static boolean isValidEmail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }


    public static void openWhatsApp(Context context, String number, String message) {
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + number + "&text=" + URLEncoder.encode(message, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
            } else {
                Toast.makeText(context, "Whatsapp not installed in your device", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("ERROR WHATSAPP", e.toString());
            Toast.makeText(context, "Whatsapp not installed in your device", Toast.LENGTH_LONG).show();
        }
    }

    public static boolean isInternetConnected(Context mContext) {
        boolean outcome = false;
        try {
            if (mContext != null) {
                ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
                for (NetworkInfo tempNetworkInfo : networkInfos) {
                    if (tempNetworkInfo.isConnected()) {
                        outcome = true;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outcome;
    }

    public static boolean isOreoOrAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public static boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


    public static String getFormattedDate(int dayOfMonth, int monthOfYear, int year) {

        String day = "";
        String month = "";

        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        } else {
            day = "" + dayOfMonth;
        }

        if (monthOfYear < 10) {
            month = "0" + monthOfYear;
        } else {
            month = "" + monthOfYear;
        }


        //String date = "" + day + "-" + month + "-" + year;
        String date = "" + year + "-" + month + "-" + day;
        Log.e("date", "selected:" + date);
        return date;
    }

    public static String getCurrentDate(String format) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String date = df.format(c);
        return date;
    }

    public static String getCurrentMonthDate(String format) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String date = df.format(c);
        return date;
    }

    public static String getCurrentMonthName(String format) {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(c.getTime());// NOW
    }

    public static String getNextMonthName(String format) {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        c.add(Calendar.MONTH, +1);// One month ago
        return sdf.format(c.getTime());
    }

    public static Date getDateFromLocalDate(LocalDate localDate) {
        int day = localDate.getDayOfMonth();
        int month = localDate.getMonthOfYear();
        int year = localDate.getYear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DATE, day);
        return calendar.getTime();
    }


    public static String getDateWithDay(Date date) {
        return getDayWithDateFormat().format(date);
    }

    public static SimpleDateFormat getDayWithDateFormat() {
        return new SimpleDateFormat(Constants.DAY_WITH_DATE_FORMAT);
    }

    public static String getDateForApi(Date date) {
        return getDateFormatForApi().format(date);
    }

    public static SimpleDateFormat getDateFormatForApi() {
        return new SimpleDateFormat(Constants.DATE_FORMAT_REVERSE);
    }

    public static String getStatus(String statusCode) {
        switch (statusCode) {
            case Constants.REQUESTED:
                return "Requested";
            case Constants.ACCEPTED:
                return "Accepted";
            case Constants.DECLINED:
                return "Declined";
            case Constants.COMPLETED:
                return "Completed";
            case Constants.WORKING:
                return "Working";
            default:
                return "";
        }
    }

    public static String getTimeFromHoursMinutes(int hr, int min) {
        return "" + (hr < 10 ? ("0" + hr) : ("" + hr)) + ":" + (min < 10 ? ("0" + min) : ("" + min)) + " " + ((hr < 12) ? "AM" : "PM");
    }


    public static String getAgoTime(String dateAndTime) {
        try {
            //2019-12-13 23:46:40
            //"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            //long time = sdf.parse("2016-01-24T16:00:00.000Z").getTime();
            long time = sdf.parse(dateAndTime).getTime();
            long now = System.currentTimeMillis();

            CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);

            return "" + ago.toString();
        } catch (Exception e) {
            Log.e("exp:", ":" + e.getMessage());
            return "";
        }
    }

    public static void downloadFileFromUrl(final String fileUrl, final String filename, final String title, final Context context, FileDownloadListener fileDownloadListener) {
        try {
            Toast.makeText(context, "Saving..", Toast.LENGTH_SHORT).show();
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<ResponseBody> call = apiService.downloadImage(fileUrl);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {

                            String folderPath = saveBytesToImage(response.body().byteStream(), title, filename);

                            if (MyUtils.checkStringValue(folderPath)) {
                                fileDownloadListener.onDownloadSuccess(folderPath);
                            }

                        } else {
                            fileDownloadListener.onDownloadFailed(Constants.ERROR_DOWNLOAD_FILE);
                            Log.e("error", "null response");
                        }
                    } else {
                        fileDownloadListener.onDownloadFailed(Constants.ERROR_DOWNLOAD_FILE);
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    fileDownloadListener.onDownloadFailed(Constants.ERROR_DOWNLOAD_FILE);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            fileDownloadListener.onDownloadFailed(Constants.ERROR_DOWNLOAD_FILE);
        }
    }

    public static String saveBytesToImage(InputStream inputStream, String title, String fileName) {
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            title = title.replaceAll(" ", "_");
            File myDir = new File(root + Constants.IMAGE_FOLDER + title);
            if (!myDir.exists()) {
                myDir.mkdirs();
            }

            File file = new File(myDir, fileName);
            if (file.exists()) {
                file.delete();
            }


            FileOutputStream outputStream = new FileOutputStream(file);

            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            outputStream.close();
            inputStream.close();

            Log.e("storageLocation", "file:" + file.getPath());

            return file.getAbsolutePath();

        } catch (IOException e) {
            Log.e("PictureDemo", "Exception in photoCallback", e);

            return null;
        } catch (Exception e) {
            Log.e("PictureDemo", "Exception in photoCallback", e);

            return null;
        }
    }

    public static class TimeAgo {
        private static final int SECOND_MILLIS = 1000;
        private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

        public static String getTimeAgo(long time) {
            if (time < 1000000000000L) {
                time *= 1000;
            }

            long now = System.currentTimeMillis();
            if (time > now || time <= 0) {
                return null;
            }


            final long diff = now - time;
            if (diff < MINUTE_MILLIS) {
                return "0m";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "1m";
            } else if (diff < 50 * MINUTE_MILLIS) {
                return diff / MINUTE_MILLIS + "m";
            } else if (diff < 90 * MINUTE_MILLIS) {
                return "1h";
            } else if (diff < 24 * HOUR_MILLIS) {
                return diff / HOUR_MILLIS + "h";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "1d";
            } else {
                return diff / DAY_MILLIS + "d";
            }
        }
    }

    public static int getVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    public static String getVersionCodeAndName() {
        return "" + BuildConfig.VERSION_NAME + "-" + BuildConfig.VERSION_CODE;
    }

    public static String getAppPackageName(Context context) {
        return context.getPackageName(); // package name of the app
    }

}
