package com.example.sciencepro.utils.custom_spinner;

public interface OnItemSelected {
    void onItemSelected(int position);
}
